import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useDispatch } from 'react-redux';
import { fetchDataFormula } from 'slices/formulaSlice';

const useFetchDataFormula = () => {
  const dispatch = useDispatch();
  const dataFormula = useSelector((state) => state.formula.dataFormula);
  const isLoading = useSelector((state) => state.formula.isLoading);
  const error = useSelector((state) => state.formula.error);

  useEffect(() => {
    dispatch(fetchDataFormula());
  }, [dispatch]);

  return { dataFormula, isLoading, error };
};

export default useFetchDataFormula;
