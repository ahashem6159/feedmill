import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { fetchDataDashboard } from 'slices/dashboardSlice';

const useFetchDashboardData = (startDate, endDate, currentURL) => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchDataDashboard({ startDate, endDate }));
  }, [startDate, endDate, currentURL]); // Including startDate in dependencies
};

export default useFetchDashboardData;
