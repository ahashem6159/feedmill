// import React, { createContext, useContext, useState } from 'react';

// const AuthContext = createContext(null);

// export const useAuth = () => useContext(AuthContext);

// export const AuthProvider = ({ children }) => {
//   const [isAuthenticated, setIsAuthenticated] = useState(false);

//   const login = () => {
//     // Implement login logic here
//     setIsAuthenticated(true);
//   };

//   const logout = () => {
//     // Implement logout logic here
//     setIsAuthenticated(false);
//   };

//   return <AuthContext.Provider value={{ isAuthenticated, login, logout }}>{children}</AuthContext.Provider>;
// };

import React, { createContext, useContext } from 'react';
// import { useLocation } from 'react-router';

const AuthContext = createContext(null);

export const useAuth = () => useContext(AuthContext);

// const usePreviousUrl = () => {
//   const [history, setHistory] = useState([]);
//   const location = useLocation();

//   useEffect(() => {
//     // Add the new location to the history array
//     setHistory((prev) => [...prev, location]);
//   }, [location]);

//   // Return the second to last location, as the last one is the current location
//   return history.length > 1 ? history[history.length - 2].pathname : null;
// };

export const AuthProvider = ({ children }) => {
  // const previousUrl = usePreviousUrl();

  // useEffect(() => {
  //   if (previousUrl === null) {
  //     window.location.href = 'https://testserver2.abuerdan.com/admin/FeedMale/';
  //   }
  // }, [previousUrl]);

  // const [data, setData] = useState(null);
  // const [isLoading, setIsLoading] = useState(false);
  // const [error, setError] = useState(null);
  // const [isAuthenticated, setIsAuthenticated] = useState(false);

  // useEffect(() => {
  //   const fetchData = async () => {
  //     setIsLoading(true);
  //     setError(null);

  //     try {
  //       const response = await fetch('https://testserver2.abuerdan.com/FeedMale/home-page/'); // Replace '/api/data' with your API endpoint
  //       if (response.ok) {
  //         const result = await response.json();
  //         setData(result);
  //         setIsAuthenticated(false);
  //       } else {
  //         setError('Failed to fetch data.');
  //         setIsAuthenticated(false);
  //       }
  //     } catch (error) {
  //       setIsAuthenticated(false);
  //       setError('An error occurred while fetching data.');
  //     }

  //     setIsLoading(false);
  //   };

  //   fetchData();
  // }, []);

  return <AuthContext.Provider value={{}}>{children}</AuthContext.Provider>;
};
