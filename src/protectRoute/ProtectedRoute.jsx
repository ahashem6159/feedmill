// import { useAuth } from 'context/Auth';
// import React from 'react';

import { useEffect } from 'react';

// import { useEffect } from 'react';

// const usePreviousUrl = () => {
//   const [history, setHistory] = useState([]);
//   const location = useLocation();

//   useEffect(() => {
//     // Add the new location to the history array
//     setHistory((prev) => [...prev, location]);
//   }, [location]);

//   // Return the second to last location, as the last one is the current location
//   return history.length > 1 ? history[history.length - 2].pathname : null;
// };

const ProtectedRoute = ({ children }) => {
  // const previousUrl = usePreviousUrl();

  // useEffect(() => {
  //   if (previousUrl === null) {
  //     window.location.href = 'https://testserver2.abuerdan.com/admin/FeedMale/';
  //   }
  // }, [previousUrl]);

  // const { previousUrl } = useAuth();

  // https://goldenchicken.abuerdan.com/

  // useEffect(() => {
  //   console.log(document.referrer);
  //   console.log(document.referrer.split('/')[2]);
  //   console.log('localhost:3000'.replace(/:.*/, ''));
  //   // if (!isAuthenticated) window.location.href = 'https://testserver2.abuerdan.com/admin/login/?next=/admin/FeedMale/';
  //   if (!document.referrer.includes(`${document.referrer.split('/')[2]}`)) {
  //     if (!document.referrer.includes(`${document.referrer.split('/')[2]?.replace(/:.*/, '')}`)) {
  //       window.location.href = `https://${document.referrer.split('/')[2]?.replace(/:.*/, '')}/?next_page=dashboard`;
  //       // window.location.href = 'https://goldenchicken.abuerdan.com/?next_page=dashboard';
  //     }
  //   }
  // }, []);

  useEffect(() => {
    console.log(document.referrer);
    console.log(document.referrer.split('/')[2]);
    console.log('localhost:3000'.replace(/:.*/, ''));
    // if (!isAuthenticated) window.location.href = 'https://testserver2.abuerdan.com/admin/login/?next=/admin/FeedMale/';
    if (!document.referrer.includes(`goldenchicken.abuerdan.com::8004`)) {
      if (!document.referrer.includes(`goldenchicken.abuerdan.com`)) {
        // window.location.href = `https://goldenchicken.abuerdan.com/?next_page=dashboard`;
        // window.location.href = `https://goldenchicken.abuerdan.com/?next_page=dashboard`;
        // window.location.href = 'https://goldenchicken.abuerdan.com/?next_page=dashboard';
      }
    }
  }, []);

  // return previousUrl !== null ? children : null;
  // return previousUrl === null ? children : null;
  // return previousUrl !== 'goldenchicken.abuerdan.com' ? children : null;

  // console.log(document.referrer);

  // console.log(window.location.state);

  // return document.referrer.includes(`${document.referrer.split('/')[2]?.replace(/:.*/, '')}`) ||
  //   document.referrer.includes(`${document.referrer.split('/')[2]}`)
  //   ? children
  //   : null;
  return !document.referrer.includes(`goldenchicken.abuerdan.com`) || !document.referrer.includes(`goldenchicken.abuerdan.com:8004`)
    ? children
    : null;

  // return document.referrer.includes('192.168.1.125:8000')
  //   ? children
  //   : (window.location.href = 'http://192.168.1.125:8000/?next_page=dashboard');
};

export default ProtectedRoute;
