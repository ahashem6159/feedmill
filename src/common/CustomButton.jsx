import React from 'react';
import Button from '@mui/material/Button';

const CustomButton = ({ title, color, onClick }) => {
  return (
    <Button className="customButtons--button" variant="outlined" color={color} onClick={onClick}>
      {title}
    </Button>
  );
};

export default CustomButton;
