import React from 'react';
// eslint-disable-next-line no-restricted-imports
import { DemoContainer } from '@mui/x-date-pickers/internals/demo';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DateTimePicker } from '@mui/x-date-pickers/DateTimePicker';
import { useState } from 'react';

const CustomDateTimePicker = () => {
  const [selectedDate, setSelectedDate] = useState(new Date());

  console.log(new Date(selectedDate).getFullYear());
  console.log(new Date(selectedDate).getDay());

  return (
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      <DemoContainer components={['DateTimePicker']}>
        <DateTimePicker
          label="Date - Time"
          onChange={(newValue) => {
            setSelectedDate(newValue);
          }}
          slotProps={{ textField: { size: 'small' } }}
        />
      </DemoContainer>
    </LocalizationProvider>
  );
};

export default CustomDateTimePicker;
