import React from 'react';

// eslint-disable-next-line no-restricted-imports
import { DemoContainer } from '@mui/x-date-pickers/internals/demo';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { TimePicker } from '@mui/x-date-pickers';

const CustomTimePicker = ({ title }) => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <p>{title}</p>
      <LocalizationProvider dateAdapter={AdapterDayjs}>
        <DemoContainer components={['DatePicker']}>
          <TimePicker slotProps={{ textField: { size: 'small', className: 'fullWidthInput' } }} />
        </DemoContainer>
      </LocalizationProvider>
    </div>
  );
};

export default CustomTimePicker;
