import React from 'react';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import PropTypes from 'prop-types';

const CustomInput = ({ title, onChange, value, type, label }) => {
  return (
    <div className="formula__information__parent__type">
      <p>{title}</p>
      <Box
        component="form"
        sx={{
          width: '100%'
          // '& > :not(style)': { m: 0, width: '100%' }
        }}
        noValidate
        autoComplete="off"
      >
        <TextField
          sx={{ width: '100%' }}
          size="small"
          id="outlined-basic"
          label={label}
          variant="outlined"
          type={type}
          value={value}
          onChange={onChange}
        />
      </Box>
    </div>
  );
};

CustomInput.prototype = {
  title: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  type: PropTypes.string.isRequired
};

export default CustomInput;
