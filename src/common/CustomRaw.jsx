import React from 'react';
import CustomInput from './CustomInput';
import { Button, Grid } from '@mui/material';
import CustomSelectSearch from './CustomSelectSearch';
import DeleteOutlineOutlinedIcon from '@mui/icons-material/DeleteOutlineOutlined';
import { useState } from 'react';

const CustomRaw = ({ headerTitle, item, setRawMaterialArray, data }) => {
  const [selectedValue, setSelectedValue] = useState(null);
  const { id, value, quantity } = item;

  const handleAutocompleteChange = (event, newValue) => {
    setSelectedValue(newValue);
    setRawMaterialArray((prevState) => prevState.map((item) => (item.id === id ? { ...item, value: newValue } : item)));
  };

  const deleteRawHandler = () => {
    setRawMaterialArray((prevState) => prevState.filter((el) => el.id !== id));
  };

  const changeItemHandler = (event) => {
    setRawMaterialArray((prevState) => prevState.map((item) => (item.id === id ? { ...item, quantity: event.target.value } : item)));
  };

  const formulaDetails = (
    <>
      <Grid item xs={5.5}>
        <CustomSelectSearch title="Ingredient Name" data={data} value={value} onChange={handleAutocompleteChange} />
      </Grid>
      <Grid item xs={5.5}>
        <CustomInput title="Quantity In KGs" type="number" label="KGs" value={quantity} onChange={changeItemHandler} />
      </Grid>
      <Grid item xs={1}>
        <Button className="deleteBtn" variant="outlined" onClick={deleteRawHandler}>
          <DeleteOutlineOutlinedIcon />
        </Button>
      </Grid>
    </>
  );

  const transportationDetails = (
    <>
      <Grid item xs={4}>
        <CustomSelectSearch title="Ingredient Name" data={data} value={selectedValue} onChange={handleAutocompleteChange} />
      </Grid>
      <Grid item xs={4}>
        <CustomInput title="Quantity In KGs" type="number" label="KGs" value={quantity} onChange={changeItemHandler} />
      </Grid>
      <Grid item xs={4}>
        <CustomInput title="Adjustment Quantity in KGs" type="number" label="KGs" value={quantity} onChange={changeItemHandler} />
      </Grid>
      <Grid item xs={1}>
        <Button className="deleteBtn" variant="outlined" onClick={deleteRawHandler}>
          <DeleteOutlineOutlinedIcon />
        </Button>
      </Grid>
    </>
  );

  return (
    <Grid item xs={12}>
      <div className="formula__information__parent">
        <Grid container spacing={0}>
          <div className="formula__information__parent--raw">
            {headerTitle === 'Transportation Raw Material' ? transportationDetails : formulaDetails}
          </div>
        </Grid>
      </div>
    </Grid>
  );
};

export default CustomRaw;
