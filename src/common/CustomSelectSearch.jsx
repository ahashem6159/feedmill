import React from 'react';
import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';

const CustomSelectSearch = ({ title, data, value, onChange, label, isAssignClasses }) => {
  return (
    <div className="formula__information__details__type" style={{ paddingBottom: `${isAssignClasses && '0px'}` }}>
      {title && <p>{title}</p>}
      <Autocomplete
        fullWidth
        size="small"
        disablePortal
        id="combo-box-demo"
        options={data}
        renderInput={(params) => <TextField {...params} label={label} />}
        value={value}
        onChange={onChange}
      />
    </div>
  );
};

export default CustomSelectSearch;
