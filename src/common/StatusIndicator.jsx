import React from 'react';

const StatusIndicator = ({ isActive }) => {
  const statusClass = isActive ? 'status active' : 'status inactive';

  console.log(isActive);

  return (
    <div className={statusClass}>
      <p>{isActive ? 'ACTIVE' : 'INACTIVE'}</p>
    </div>
  );
};

export default StatusIndicator;
