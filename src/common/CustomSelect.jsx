import React from 'react';

import PropTypes from 'prop-types'; // Import PropTypes

// MUI
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import { InputLabel } from '@mui/material';
import OutlinedInput from '@mui/material/OutlinedInput';
import MenuItem from '@mui/material/MenuItem';

const CustomSelect = ({ title, label, value, onChange, names }) => {
  return (
    <div className="formula__information__details__type">
      <p>{title}</p>
      <FormControl fullWidth size="small">
        <InputLabel id="demo-multiple-name-label">{label}</InputLabel>
        <Select
          labelId="demo-multiple-name-label"
          id="demo-multiple-name"
          multiple
          value={value}
          onChange={onChange}
          input={<OutlinedInput label="Name" />}
          noValidate
          autoComplete="off"
        >
          {names.map((name) => (
            <MenuItem key={name} value={name}>
              {name}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </div>
  );
};

CustomSelect.propTypes = {
  title: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  value: PropTypes.array.isRequired,
  onChange: PropTypes.func.isRequired,
  names: PropTypes.arrayOf(PropTypes.string).isRequired
};

export default CustomSelect;
