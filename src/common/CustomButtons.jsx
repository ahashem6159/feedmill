import React from 'react';
import CustomButton from './CustomButton';

const CustomButtons = ({ id, deleteHandler }) => {
  return (
    <div className="customButtons">
      <CustomButton title="Save" />
      {id && <CustomButton color="error" title="Delete" onClick={deleteHandler} />}
    </div>
  );
};

export default CustomButtons;
