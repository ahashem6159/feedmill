import { configureStore } from '@reduxjs/toolkit';
import customizeSlice from './customizeSlice';
import dashboardSlice from 'slices/dashboardSlice';
import formulaSlice from 'slices/formulaSlice';

const store = configureStore({
  reducer: { customize: customizeSlice, dashboard: dashboardSlice, formula: formulaSlice }
});

export default store;
