// project imports
import { createSlice } from '@reduxjs/toolkit';
import config from 'config';

export const initialState = {
  isOpen: [], // for active default menu
  fontFamily: config.fontFamily,
  borderRadius: config.borderRadius,
  opened: false
};

const customizeSlice = createSlice({
  name: 'customize',
  initialState,
  reducers: {
    menuOpen(state, action) {
      state.isOpen = [action.payload];
    },
    setMenu(state) {
      state.opened = false;
    },
    updateFontFamily(state, action) {
      state.opened = action.payload;
    },
    updateBorderRadius(state, action) {
      state.opened = action.payload;
    }
  }
});

export const { menuOpen, setMenu, updateFontFamily, updateBorderRadius } = customizeSlice.actions;

export default customizeSlice.reducer;
