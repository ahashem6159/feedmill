import { lazy } from 'react';

// project imports
import MainLayout from 'layout/MainLayout';
import Loadable from 'ui-component/Loadable';
// import Farms from 'views/management/Farms';
import MaterialStock from 'views/management/materialStock/MaterialStock';
import Sections from 'views/management/Sections';
import Brands from 'views/management/Brands';
import CreateFormula from 'views/management/formula/CreateFormula';
import CreateStock from 'views/management/materialStock/CreateStock';
import Transportation from 'views/management/transportation/Transportation';
import CreateTransportation from 'views/management/transportation/CreateTransportation';
import Production from 'views/management/production/Production';
import CreateProduction from 'views/management/production/CreateProduction';
import Driver from 'views/management/driver/Driver';
import CreateDriver from 'views/management/driver/CreateDriver';
import BreedersExternalFarm from 'views/management/breedersExternalFarm/BreedersExternalFarm';
import CreateBreaderExternalFarm from 'views/management/breedersExternalFarm/CreateBreaderExternalFarm';
import BroilersExternalFarm from 'views/management/broilersExternalFarm/BroilersExternalFarm';
import CreateBroilersExternalFarm from 'views/management/broilersExternalFarm/CreateBroilersExternalFarm';
import FeedSupplier from 'views/management/feedSupplier/FeedSupplier';
import CreateFeedSupplier from 'views/management/feedSupplier/CreateFeedSupplier';
import FeedType from 'views/management/feedType/FeedType';
import CreateFeedType from 'views/management/feedType/CreateFeedType';
import MannerDelivery from 'views/management/mannerDelivery/MannerDelivery';
import CreateMannerDelivery from 'views/management/mannerDelivery/CreateMannerDelivery';
import MedicationDelivery from 'views/management/medicationDelivery/MedicationDelivery';
import CreateMedicationDelivery from 'views/management/medicationDelivery/CreateMedicationDelivery';
import CreateMedicationInfo from 'views/management/medicationInfo/CreateMedicationInfo';
import MedicationInfo from 'views/management/medicationInfo/MedicationInfo';
import CreateRawMaterialStock from 'views/management/rowMaterialStock/CreateRawMaterialStock';
import RawMaterialStock from 'views/management/rowMaterialStock/RawMaterialStock';
import RawMaterialSupplier from 'views/management/rawMaterialSupplier/RawMaterialSupplier';
import CreateRawMaterialSupplier from 'views/management/rawMaterialSupplier/CreateRawMaterialSupplier';
import CreateReason from 'views/management/reason/CreateReason';
import Reason from 'views/management/reason/Reason';
import TransportationFeedDelivery from 'views/management/transportationFeedDelivery/TransportationFeedDelivery';
import CreateTransportationFeedDelivery from 'views/management/transportationFeedDelivery/CreateTransportationFeedDelivery';
import Truck from 'views/management/truck/Truck';
import CreateTruck from 'views/management/truck/CreateTruck';
import BroilersFeedRequests from 'views/management/broilersFeedRequests/BroilersFeedRequests';
import CreateBroilersFeedRequests from 'views/management/broilersFeedRequests/CreateBroilersFeedRequests';
import BreedersFeedRequests from 'views/management/breedersFeedRequests/BreedersFeedRequests';
import CreateBreedersFeedRequests from 'views/management/breedersFeedRequests/CreateBreedersFeedRequests';
import PageNotFound from 'views/pages/authentication/PageNotFound';
import ProtectedRoute from 'protectRoute/ProtectedRoute';

// dashboard routing
const DashboardDefault = Loadable(lazy(() => import('views/dashboard/Default')));

// utilities routing
const UtilsTypography = Loadable(lazy(() => import('views/utilities/Typography')));
const UtilsColor = Loadable(lazy(() => import('views/utilities/Color')));
const UtilsShadow = Loadable(lazy(() => import('views/utilities/Shadow')));
const Formula = Loadable(lazy(() => import('views/management/formula/Formula')));
// const UtilsMaterialIcons = Loadable(lazy(() => import('views/utilities/MaterialIcons')));
// const UtilsTablerIcons = Loadable(lazy(() => import('views/utilities/TablerIcons')));

// sample page routing
// const SamplePage = Loadable(lazy(() => import('views/sample-page')));

// ==============================|| MAIN ROUTING ||============================== //

const MainRoutes = {
  // path: '/',
  // element: <MainLayout />,
  path: '/',
  element: (
    <ProtectedRoute>
      <MainLayout />
    </ProtectedRoute>
  ),
  children: [
    {
      path: '/',
      element: (
        <ProtectedRoute>
          <DashboardDefault />
        </ProtectedRoute>
      )
    },
    {
      path: 'dashboard',
      children: [
        {
          path: 'default',
          element: <DashboardDefault />
        }
      ]
    },
    {
      path: 'utils',
      children: [
        {
          path: 'util-typography',
          element: <UtilsTypography />
        }
      ]
    },
    {
      path: 'utils',
      children: [
        {
          path: 'util-color',
          element: <UtilsColor />
        }
      ]
    },
    {
      path: 'utils',
      children: [
        {
          path: 'util-shadow',
          element: <UtilsShadow />
        }
      ]
    },
    // {
    //   path: 'breeders',
    //   children: [
    //     {
    //       path: 'farms',
    //       element: <Farms />
    //     }
    //   ]
    // },
    {
      path: 'administration',
      children: [
        {
          path: 'formula',
          element: <Formula />
        },
        {
          path: 'formula/edit/:id',
          element: <CreateFormula />
        },
        {
          path: 'formula/create/',
          element: <CreateFormula />
        }
      ]
    },
    {
      path: 'material',
      children: [
        {
          path: 'Material-Stock',
          element: <MaterialStock />
        },
        {
          path: 'Material-Stock/edit/:id',
          element: <CreateStock />
        },
        {
          path: 'Material-Stock/create/',
          element: <CreateStock />
        }
      ]
    },
    {
      path: 'administration',
      children: [
        {
          path: 'Transportation',
          element: <Transportation />
        },
        {
          path: 'Transportation/edit/:id',
          element: <CreateTransportation />
        },
        {
          path: 'Transportation/create/',
          element: <CreateTransportation />
        }
      ]
    },
    {
      path: 'administration',
      children: [
        {
          path: 'Production',
          element: <Production />
        },
        {
          path: 'Production/edit/:id',
          element: <CreateProduction />
        },
        {
          path: 'Production/create/',
          element: <CreateProduction />
        }
      ]
    },
    {
      path: 'administration',
      children: [
        {
          path: 'Drivers',
          element: <Driver />
        },
        {
          path: 'Drivers/edit/:id',
          element: <CreateDriver />
        },
        {
          path: 'Drivers/create/',
          element: <CreateDriver />
        }
      ]
    },
    {
      path: 'reports',
      children: [
        {
          path: 'FeedSupplier',
          element: <FeedSupplier />
        },
        {
          path: 'FeedSupplier/edit/:id',
          element: <CreateFeedSupplier />
        },
        {
          path: 'FeedSupplier/create/',
          element: <CreateFeedSupplier />
        }
      ]
    },
    {
      path: 'reports',
      children: [
        {
          path: 'FeedType',
          element: <FeedType />
        },
        {
          path: 'FeedType/edit/:id',
          element: <CreateFeedType />
        },
        {
          path: 'FeedType/create/',
          element: <CreateFeedType />
        }
      ]
    },

    {
      path: 'administration',
      children: [
        {
          path: 'MedicationInfo',
          element: <MedicationInfo />
        },
        {
          path: 'MedicationInfo/edit/:id',
          element: <CreateMedicationInfo />
        },
        {
          path: 'MedicationInfo/create/',
          element: <CreateMedicationInfo />
        }
      ]
    },
    {
      path: 'material',
      children: [
        {
          path: 'RawMaterialStock',
          element: <RawMaterialStock />
        },
        {
          path: 'RawMaterialStock/edit/:id',
          element: <CreateRawMaterialStock />
        },
        {
          path: 'RawMaterialStock/create/',
          element: <CreateRawMaterialStock />
        }
      ]
    },
    {
      path: 'material',
      children: [
        {
          path: 'RawMaterialSupplier',
          element: <RawMaterialSupplier />
        },
        {
          path: 'RawMaterialSupplier/edit/:id',
          element: <CreateRawMaterialSupplier />
        },
        {
          path: 'RawMaterialSupplier/create/',
          element: <CreateRawMaterialSupplier />
        }
      ]
    },
    {
      path: 'reports',
      children: [
        {
          path: 'Reason',
          element: <Reason />
        },
        {
          path: 'Reason/edit/:id',
          element: <CreateReason />
        },
        {
          path: 'Reason/create/',
          element: <CreateReason />
        }
      ]
    },

    {
      path: 'reports',
      children: [
        {
          path: 'Truck',
          element: <Truck />
        },
        {
          path: 'Truck/edit/:id',
          element: <CreateTruck />
        },
        {
          path: 'Truck/create/',
          element: <CreateTruck />
        }
      ]
    },
    {
      path: 'reports',
      children: [
        {
          path: 'sections',
          element: <Sections />
        }
      ]
    },
    {
      path: 'reports',
      children: [
        {
          path: 'brands',
          element: <Brands />
        }
      ]
    },
    {
      path: 'delivery',
      children: [
        {
          path: 'MannerDelivery',
          element: <MannerDelivery />
        },
        {
          path: 'MannerDelivery/edit/:id',
          element: <CreateMannerDelivery />
        },
        {
          path: 'MannerDelivery/create/',
          element: <CreateMannerDelivery />
        }
      ]
    },
    {
      path: 'delivery',
      children: [
        {
          path: 'MedicationDelivery',
          element: <MedicationDelivery />
        },
        {
          path: 'MedicationDelivery/edit/:id',
          element: <CreateMedicationDelivery />
        },
        {
          path: 'MedicationDelivery/create/',
          element: <CreateMedicationDelivery />
        }
      ]
    },
    {
      path: 'delivery',
      children: [
        {
          path: 'TransportationFeedDelivery',
          element: <TransportationFeedDelivery />
        },
        {
          path: 'TransportationFeedDelivery/edit/:id',
          element: <CreateTransportationFeedDelivery />
        },
        {
          path: 'TransportationFeedDelivery/create/',
          element: <CreateTransportationFeedDelivery />
        }
      ]
    },
    {
      path: 'breeders',
      children: [
        {
          path: 'BreedersExternalFarm',
          element: <BreedersExternalFarm />
        },
        {
          path: 'BreedersExternalFarm/edit/:id',
          element: <CreateBreaderExternalFarm />
        },
        {
          path: 'BreedersExternalFarm/create/',
          element: <CreateBreaderExternalFarm />
        }
      ]
    },
    {
      path: 'broilers',
      children: [
        {
          path: 'BroilersExternalFarm',
          element: <BroilersExternalFarm />
        },
        {
          path: 'BroilersExternalFarm/edit/:id',
          element: <CreateBroilersExternalFarm />
        },
        {
          path: 'BroilersExternalFarm/create/',
          element: <CreateBroilersExternalFarm />
        }
      ]
    },
    {
      path: 'breeders',
      children: [
        {
          path: 'BreedersFeedRequests',
          element: <BreedersFeedRequests />
        },
        {
          path: 'BreedersFeedRequests/edit/:id',
          element: <CreateBreedersFeedRequests />
        },
        {
          path: 'BreedersFeedRequests/create/',
          element: <CreateBreedersFeedRequests />
        }
      ]
    },
    {
      path: 'broilers',
      children: [
        {
          path: 'BroilersFeedRequests',
          element: <BroilersFeedRequests />
        },
        {
          path: 'BroilersFeedRequests/edit/:id',
          element: <CreateBroilersFeedRequests />
        },
        {
          path: 'BroilersFeedRequests/create/',
          element: <CreateBroilersFeedRequests />
        }
      ]
    },
    // {
    //   path: 'sample-page',
    //   element: <SamplePage />
    // }
    {
      path: '*',
      element: <PageNotFound />
    }
  ]
};

export default MainRoutes;
