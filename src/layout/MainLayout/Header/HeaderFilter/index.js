import { useState, useRef, useEffect } from 'react';

// material-ui
import { useTheme } from '@mui/material/styles';
import { Avatar, Box, ButtonBase, ClickAwayListener, Grid, Paper, Popper, useMediaQuery } from '@mui/material';
import FilterAltIcon from '@mui/icons-material/FilterAlt';
// import dayjs from 'dayjs';

// third-party

// project imports
import MainCard from 'ui-component/cards/MainCard';
import Transitions from 'ui-component/extended/Transitions';
import { DatePicker, Form, Space } from 'antd';
import useFetchDashboardData from 'hooks/useFetchDashboardData ';
import moment from 'moment';

// ==============================|| NOTIFICATION ||============================== //

const HeaderFilter = () => {
  const { RangePicker } = DatePicker;
  const [form] = Form.useForm();
  const theme = useTheme();
  const currentURL = window.location.href;

  const originalDate = new Date();
  const year = originalDate.getFullYear();
  const month = originalDate.getMonth() + 1; // Months are 0-based, so add 1
  const day = originalDate.getDate();

  // Convert to the desired format
  const formattedDate = moment(`${year}-${month.toString().padStart(2, '0')}-${day.toString().padStart(2, '0')}`);

  const [dates, setDates] = useState([moment(formattedDate), moment(formattedDate)]);

  const handleDateChange = (dates) => {
    setDates(dates);
  };

  const disabledDate = (current) => {
    // Can not select days after today
    return current && current > moment().endOf('day');
  };

  let startDate;
  let endDate;
  if (dates) {
    startDate = dates[0]?.format('YYYY-MM-DD');
    endDate = dates[1]?.format('YYYY-MM-DD');
    // startDate = new Date(dates?.[0]).toISOString();
    // endDate = new Date(dates?.[1]).toISOString();
  }

  const matchesXs = useMediaQuery(theme.breakpoints.down('md'));

  const [open, setOpen] = useState(false);

  const anchorRef = useRef(null);

  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen);
  };

  const handleClose = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }
    setOpen(false);
  };

  const prevOpen = useRef(open);
  useEffect(() => {
    if (prevOpen.current === true && open === false) {
      anchorRef.current.focus();
    }
    prevOpen.current = open;
  }, [open]);

  // useEffect(() => {
  //   dispatch(fetchDataDashboard({ startDate, endDate }));
  // }, [endDate, currentURL]);

  useFetchDashboardData(startDate, endDate, currentURL);

  const chooseDate = dates && (
    <div className="dateContent">
      <button onClick={handleToggle}>
        <span>Start Date</span>: {dates[0].format('YYYY-MM-DD')}
      </button>
      <button onClick={handleToggle}>
        <span>End Date</span>: {dates[1].format('YYYY-MM-DD')}
      </button>
    </div>
  );

  // const onSubmit = (values) => {
  //   console.log('Received values: ', values);
  //   console.log(values);
  // };

  return (
    <>
      {chooseDate}
      <Box
        sx={{
          ml: 2,
          mr: 1,
          [theme.breakpoints.down('md')]: {
            mr: 2
          }
        }}
      >
        <ButtonBase sx={{ borderRadius: '12px' }}>
          <Avatar
            variant="rounded"
            sx={{
              ...theme.typography.commonAvatar,
              ...theme.typography.mediumAvatar,
              transition: 'all .2s ease-in-out',
              background: theme.palette.secondary.light,
              color: theme.palette.secondary.dark,
              '&[aria-controls="menu-list-grow"],&:hover': {
                background: theme.palette.secondary.dark,
                color: theme.palette.secondary.light
              }
            }}
            ref={anchorRef}
            aria-controls={open ? 'menu-list-grow' : undefined}
            aria-haspopup="true"
            onClick={handleToggle}
            color="inherit"
          >
            <FilterAltIcon stroke={1.5} size="1.3rem" />
          </Avatar>
        </ButtonBase>
      </Box>
      <Popper
        placement={matchesXs ? 'bottom' : 'bottom-end'}
        open={open}
        anchorEl={anchorRef.current}
        role={undefined}
        transition
        disablePortal
        popperOptions={{
          modifiers: [
            {
              name: 'offset',
              options: {
                offset: [matchesXs ? 5 : 0, 20]
              }
            }
          ]
        }}
      >
        {({ TransitionProps }) => (
          <Transitions position={matchesXs ? 'top' : 'top-right'} in={open} {...TransitionProps}>
            <Paper>
              <ClickAwayListener onClickAway={handleClose}>
                <MainCard border={false} elevation={16} content={false} boxShadow shadow={theme.shadows[16]}>
                  <Grid container direction="column" spacing={2}>
                    <Grid item xs={12}>
                      <Grid container alignItems="center" justifyContent="space-between" sx={{ pt: 2, px: 2 }}>
                        <Grid item xs={12}>
                          <Space direction="vertical" size={12}>
                            <Form form={form}>
                              <Form.Item
                                style={{ marginBottom: '0px' }}
                                name="dateRange"
                                // label="Select Date Range"
                                // rules={[{ required: true, message: 'Please select a date range!' }]}
                              >
                                <RangePicker
                                  // initialValues={dayjs('2015-01-01', 'YYYY-MM-DD')}
                                  onChange={handleDateChange}
                                  value={dates}
                                  disabledDate={disabledDate}
                                  format="YYYY-MM-DD"
                                />
                              </Form.Item>
                            </Form>
                          </Space>
                        </Grid>
                      </Grid>
                    </Grid>
                    <Grid item xs={12}></Grid>
                  </Grid>
                </MainCard>
              </ClickAwayListener>
            </Paper>
          </Transitions>
        )}
      </Popper>
    </>
  );
};

export default HeaderFilter;
