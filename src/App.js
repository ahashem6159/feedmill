import { useSelector } from 'react-redux';

import { ThemeProvider } from '@mui/material/styles';
import { CssBaseline, StyledEngineProvider } from '@mui/material';
// import { TooltipComponent } from '@syncfusion/ej2-react-popups';
// import { FiSettings } from 'react-icons/fi';

// routing
import Routes from 'routes';

// defaultTheme
import themes from 'themes';

// project imports
import NavigationScroll from 'layout/NavigationScroll';
import { useStateContext } from 'context/ContextProvider';
import { useEffect } from 'react';
// import ThemeSettings from 'ui-component/ThemeSettings';

// ==============================|| APP ||============================== //

const App = () => {
  const customize = useSelector((state) => state.customize);
  const { setCurrentColor, setCurrentMode, currentMode } = useStateContext();
  // const { setCurrentColor, setCurrentMode, currentMode, currentColor, themeSettings, setThemeSettings } = useStateContext();

  useEffect(() => {
    const currentThemeColor = localStorage.getItem('colorMode');
    const currentThemeMode = localStorage.getItem('themeMode');
    if (currentThemeColor && currentThemeMode) {
      setCurrentColor(currentThemeColor);
      setCurrentMode(currentThemeMode);
    }
  }, []);

  return (
    <div className={currentMode === 'Dark' ? 'dark' : ''}>
      {/* <div className="fixed right-4 bottom-4" style={{ zIndex: '1000' }}>
        <TooltipComponent content="Settings" position="Top">
          <button
            type="button"
            onClick={() => setThemeSettings(!themeSettings)}
            style={{ background: currentColor, borderRadius: '50%' }}
            className="text-3xl text-white p-3 hover:drop-shadow-xl hover:bg-light-gray"
          >
            <FiSettings />
          </button>
        </TooltipComponent>
      </div> */}
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={themes(customize)}>
          <CssBaseline />
          <NavigationScroll>
            {/* {themeSettings && <ThemeSettings />} */}
            <Routes />
          </NavigationScroll>
        </ThemeProvider>
      </StyledEngineProvider>
    </div>
  );
};

export default App;
