import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import { constructDataFetchUrl } from '../api/dashboardApi';

export const initialState = {
  dataDashboard: [],
  isLoading: false,
  error: null
};

export const fetchDataDashboard = createAsyncThunk('dashboard/fetchData', async (dateFilters = {}, { rejectWithValue }) => {
  try {
    const url = constructDataFetchUrl(dateFilters);
    const response = await axios.get(url);
    return response.data;
  } catch (error) {
    if (error.response) {
      return rejectWithValue(error.response.data.message || error.message);
    } else {
      console.error('Unexpected error:', error);
      return rejectWithValue(error.message);
      // throw error;
    }
  }
});

export const dashboardSlice = createSlice({
  name: 'dashboard',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchDataDashboard.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(fetchDataDashboard.fulfilled, (state, action) => {
        state.isLoading = false;
        state.error = null;
        state.dataDashboard = action.payload;
      })
      .addCase(fetchDataDashboard.rejected, (state, action) => {
        state.isLoading = false;
        state.error = action.payload;
      });
  }
});

export default dashboardSlice.reducer;
