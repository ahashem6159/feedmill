import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';

export const initialState = {
  dataFormula: {},
  currentFormula: {},
  isLoading: false,
  error: null
};

export const FORMULA_URL = 'https://testserver2.abuerdan.com/FeedMale/formula/';
// export const FORMULA_URL = 'http://192.168.1.125:8000/FeedMale/formula/';

export const fetchDataFormula = createAsyncThunk('formula/fetchData', async (_, { rejectWithValue }) => {
  try {
    const response = await axios.get(`${FORMULA_URL}`);
    return response.data;
  } catch (error) {
    console.log(error);
    if (error.response) {
      return rejectWithValue(error.response.data.message || error.message);
    } else {
      return rejectWithValue(error.message);
    }
  }
});
export const getFormula = createAsyncThunk('formula/fetchFormula', async (id = null, { rejectWithValue }) => {
  try {
    const response = await axios.get(`${FORMULA_URL}${id}`);
    return response.data;
  } catch (error) {
    if (error.response) {
      return rejectWithValue(error.response.data.message || error.message);
    } else {
      return rejectWithValue(error.message);
    }
  }
});

export const formulaSlice = createSlice({
  name: 'formula',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchDataFormula.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(fetchDataFormula.fulfilled, (state, action) => {
        state.isLoading = false;
        state.error = null;
        state.dataFormula = action.payload;
      })
      .addCase(fetchDataFormula.rejected, (state, action) => {
        state.isLoading = false;
        state.error = action.payload;
      });
    builder
      .addCase(getFormula.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(getFormula.fulfilled, (state, action) => {
        state.isLoading = false;
        state.error = null;
        state.currentFormula = action.payload;
      })
      .addCase(getFormula.rejected, (state, action) => {
        state.isLoading = false;
        state.error = action.payload;
      });
  }
});

export default formulaSlice.reducer;
