import React from 'react';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import drilldown from 'highcharts/modules/drilldown';
drilldown(Highcharts);

const MarketCharts = () => {
  const chartConfig = {
    chart: {
      type: 'column'
    },
    title: {
      align: 'left',
      text: 'Feed Deliveries / Type'
    },
    accessibility: {
      announceNewData: {
        enabled: true
      }
    },
    xAxis: {
      type: 'category',
      labels: {
        style: {
          // fontSize: '12px'
        }
      }
    },
    yAxis: {
      title: {
        text: 'Total Deliveries market share'
      }
    },
    legend: {
      enabled: false
    },
    plotOptions: {
      series: {
        borderWidth: 0,
        dataLabels: {
          enabled: true,
          format: '{point.y:.1f}%'
        }
      }
    },

    tooltip: {
      headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
      pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
    },

    series: [
      {
        name: 'Deliveries',
        colorByPoint: true,
        data: [
          {
            name: 'Starter',
            y: 63.06,
            drilldown: 'Starter'
          },
          {
            name: 'Grower',
            y: 19.84,
            drilldown: 'Grower'
          },
          {
            name: 'Finisher',
            y: 4.18,
            drilldown: 'Finisher'
          },
          {
            name: 'Broiler Starter',
            y: 4.12,
            drilldown: 'Broiler Starter'
          },
          {
            name: 'Breeder Starter Mash',
            y: 2.33,
            drilldown: 'Breeder Starter Mash'
          },
          {
            name: 'Breeder Grower Mash',
            y: 0.45,
            drilldown: 'Breeder Grower Mash'
          },
          {
            name: 'Breeder Production 01 Mash',
            y: 1.582,
            drilldown: 'Breeder Production 01 Mash'
          },
          {
            name: 'Breeder Production 02 Mash',
            y: 1.582,
            drilldown: 'Breeder Production 02 Mash'
          },
          {
            name: 'Breeder Male Mash',
            y: 1.582,
            drilldown: 'Breeder Male Mash'
          }
        ]
      }
    ],
    drilldown: {
      breadcrumbs: {
        position: {
          align: 'right'
        }
      },
      series: [
        {
          name: 'Starter',
          id: 'Starter',
          data: [
            ['Farm 1', 0.1],
            ['Farm 8', 53.02],
            ['Farm 5', 1.3]
          ]
        },
        {
          name: 'Grower',
          id: 'Grower',
          data: [
            ['Farm 12', 0.1],
            ['Farm 23', 1.3],
            ['Farm 3', 53.02]
          ]
        },
        {
          name: 'Finisher',
          id: 'Finisher',
          data: [
            ['Farm 16', 3.1],
            ['Farm 7', 22.02],
            ['Farm 1', 32.02],
            ['Farm 3', 12.02]
          ]
        },
        {
          name: 'Broiler Starter',
          id: 'Broiler Starter',
          data: [
            ['Farm 9', 30.1],
            ['Farm 1', 19.02],
            ['Farm 1', 27.02]
          ]
        },
        {
          name: 'Breeder Starter Mash',
          id: 'Breeder Starter Mash',
          data: [
            ['Farm 9', 42.1],
            ['Farm 1', 56.02]
          ]
        },
        {
          name: 'Breeder Grower Mash',
          id: 'Breeder Grower Mash',
          data: [
            ['Farm 31', 12.1],
            ['Farm 99', 39.02],
            ['Farm 76', 89.02]
          ]
        },
        {
          name: 'Breeder Production 01 Mash',
          id: 'Breeder Production 01 Mash',
          data: [
            ['Farm 12', 72.1],
            ['Farm 22', 16.02]
          ]
        },
        {
          name: 'Breeder Production 02 Mash',
          id: 'Breeder Production 02 Mash',
          data: [
            ['Farm 11', 32.1],
            ['Farm 27', 86.02]
          ]
        },
        {
          name: 'Breeder Male Mash',
          id: 'Breeder Male Mash',
          data: [
            ['Farm 4', 42.1],
            ['Farm 1', 56.02]
          ]
        }
      ]
    },

    responsive: {
      rules: [
        {
          condition: {
            maxWidth: 500
          },
          chartOptions: {
            legend: {
              layout: 'horizontal',
              align: 'center',
              verticalAlign: 'bottom'
            }
          }
        }
      ]
    }
  };

  return (
    <div>
      <HighchartsReact highcharts={Highcharts} options={chartConfig} />
    </div>
  );
};

export default MarketCharts;
