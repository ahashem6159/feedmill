import PropTypes from 'prop-types';
import { styled } from '@mui/material/styles';
import MainCard from 'ui-component/cards/MainCard';
import TotalIncomeCard from 'ui-component/cards/Skeleton/TotalIncomeCard';
import { Grid } from '@mui/material';
import { gridSpacing } from 'store/constant';

const CardWrapper = styled(MainCard)(({ theme }) => ({
  overflow: 'hidden',
  marginBottom: '10px',
  position: 'relative',
  '&:after': {
    content: '""',
    position: 'absolute',
    width: 210,
    height: 210,
    background: `linear-gradient(210.04deg, ${theme.palette.secondary.dark} -50.94%, rgba(144, 202, 249, 0) 83.49%)`,
    borderRadius: '50%',
    top: -30,
    right: -180
  },
  '&:before': {
    content: '""',
    position: 'absolute',
    width: 210,
    height: 210,
    background: `linear-gradient(140.9deg, ${theme.palette.secondary.dark} -14.02%, rgba(144, 202, 249, 0) 70.50%)`,
    borderRadius: '50%',
    top: -160,
    right: -130
  }
}));

const TonsCard = ({ isLoading, title, nameChart, color, price, percentage }) => {
  return (
    <>
      {isLoading ? (
        <TotalIncomeCard />
      ) : (
        <CardWrapper border={false} content={false} sx={{ paddingY: '6px', background: title === nameChart ? '#f1f1f1' : '' }}>
          <Grid container spacing={gridSpacing}>
            <Grid item xs={12}>
              <Grid container spacing={gridSpacing}>
                <Grid item xs={12} md={6}>
                  <h6 className="tonTitle">
                    <span className={`color-${color}`}></span>
                    {title}
                  </h6>
                </Grid>
                <Grid item xs={12} md={6}>
                  <div className="Details">
                    <span>{price}</span>
                    <span className="Details__Precentage">{percentage}</span>
                  </div>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </CardWrapper>
      )}
    </>
  );
};

TonsCard.propTypes = {
  isLoading: PropTypes.bool
};

export default TonsCard;
