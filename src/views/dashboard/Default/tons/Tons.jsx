import React from 'react';
import TonsCard from './TonsCard';
// import AgricultureIcon from '@mui/icons-material/Agriculture';

const Tons = ({ nameChart }) => {
  return (
    <div className="tons">
      <div className="tons__content">
        <div className="tons__Details">
          <h2>Total Tons</h2>
          <h1>20,000</h1>
        </div>
        <div>
          <h2>Deliveries</h2>
          <h1>12</h1>
        </div>
      </div>
      <div>
        <TonsCard nameChart={nameChart} color="Starter" title="Starter" price="2000" percentage="40.02%" />
        <TonsCard nameChart={nameChart} color="Grower" title="Grower" price="5000" percentage="26.9%" />
        <TonsCard nameChart={nameChart} color="Finisher" title="Finisher" price="8000" percentage="1.09%" />
        <TonsCard nameChart={nameChart} color="BroilerStarter" title="Broiler Starter" price="8000" percentage="15.5%" />
        <TonsCard nameChart={nameChart} color="BreederStarterMash" title="Breeder Starter Mash" price="8000" percentage="1.68%" />
        <TonsCard nameChart={nameChart} color="BreederGrowerMash" title="Breeder Grower Mash" price="8000" percentage="4.68%" />
        <TonsCard
          nameChart={nameChart}
          color="BreederProduction01Mash"
          title="Breeder Production 01 Mash"
          price="8000"
          percentage="3.68%"
        />
        <TonsCard nameChart={nameChart} color="BreederProduction02Mash" title="Breeder Production 02 Mash" price="8000" percentage="3.0%" />
        <TonsCard nameChart={nameChart} color="BreederMaleMash" title="Breeder Male Mash" price="8000" percentage="3.0%" />
      </div>
    </div>
  );
};

export default Tons;
