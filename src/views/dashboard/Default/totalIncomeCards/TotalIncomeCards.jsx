import { Grid } from '@mui/material';
import React from 'react';
import { gridSpacing } from 'store/constant';
import TotalIncomeLightCard from '../TotalIncomeLightCard';
import WorkingHour from './WorkingHour';
import DownTime from './DownTime';

const TotalIncomeCards = () => {
  return (
    <Grid item xs={12}>
      <Grid container spacing={gridSpacing}>
        <Grid item lg={4} md={12} sm={12} xs={12}>
          <TotalIncomeLightCard />
        </Grid>
        <Grid item lg={4} md={12} sm={12} xs={12}>
          <WorkingHour />
        </Grid>
        <Grid item lg={4} md={12} sm={12} xs={12}>
          <DownTime />
        </Grid>
      </Grid>
    </Grid>
  );
};

export default TotalIncomeCards;
