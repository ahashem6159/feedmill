import { styled } from '@mui/material/styles';
import MainCard from 'ui-component/cards/MainCard';
import QueryBuilderIcon from '@mui/icons-material/QueryBuilder';

const CardWrapper = styled(MainCard)(({ theme }) => ({
  overflow: 'hidden',
  position: 'relative',
  '&:after': {
    content: '""',
    position: 'absolute',
    width: 210,
    height: 210,
    background: `linear-gradient(210.04deg, ${theme.palette.warning.dark} -50.94%, rgba(144, 202, 249, 0) 83.49%)`,
    borderRadius: '50%',
    top: -30,
    right: -180
  },
  '&:before': {
    content: '""',
    position: 'absolute',
    width: 210,
    height: 210,
    background: `linear-gradient(140.9deg, ${theme.palette.warning.dark} -14.02%, rgba(144, 202, 249, 0) 70.50%)`,
    borderRadius: '50%',
    top: -160,
    right: -130
  }
}));

const WorkingHour = () => {
  return (
    <CardWrapper border={false} content={false}>
      <div className="totalIncomeCard">
        <div className="totalIncomeCard__details__hours">
          <h5 className="totalIncomeCard__details__hours--hour">
            <QueryBuilderIcon className="hourIcon" /> Total Working Hours
          </h5>
          <p>
            <span className="totalIncomeCard__details__hours--tons">23</span>
            <span>/Hr</span>
          </p>
        </div>
        <div className="totalIncomeCard__details__days">
          <h5 className="totalIncomeCard__details__hours--day">
            <QueryBuilderIcon className="dayIcon" /> Total Runtime/Hour
          </h5>
          <p>
            <span className="totalIncomeCard__details__hours--tons">44</span>
            <span>/Hr</span>
          </p>
        </div>
      </div>
    </CardWrapper>
  );
};

export default WorkingHour;
