// import { Grid } from '@mui/material';
// import React from 'react';
// import OrderProgress from './OrderProgress';

// const OrderDetails = ({ order }) => {
//   const percentage = Math.floor((order.sum_delivery_quantity / order.total_request) * 100);

//   return (
//     <div className="orders__details">
//       <Grid container spacing={0}>
//         <Grid item xs={12} md={3}>
//           <span>{order.fk_to_broilers_farm__name || order.fk_to_breeders_farm__name}</span>
//         </Grid>
//         <Grid item xs={12} md={3}>
//           <span>{order.fk_feed_type__type}</span>
//         </Grid>
//         <Grid item xs={12} md={3}>
//           {/* <span>{Math.floor(order.sum_delivery_quantity)}</span> */}
//           <span>{order.total_request}</span>
//         </Grid>
//         <Grid item xs={12} md={3}>
//           <OrderProgress percentage={percentage} color={`${order.color}`} />
//         </Grid>
//       </Grid>
//     </div>
//   );
// };

// export default OrderDetails;

// import { Grid } from '@mui/material';
// import React from 'react';
// import OrderProgress from './OrderProgress';

// const OrderDetails = ({ order }) => {
//   const percentage = order.total_request > 0 ? Math.floor((order.sum_delivery_quantity / order.total_request) * 100) : 0; // Handling division by zero

//   return (
//     <div className="orders__details">
//       <Grid container spacing={0}>
//         <Grid item xs={12} md={3}>
//           <span>{order.fk_to_broilers_farm__name || order.fk_to_breeders_farm__name}</span>
//         </Grid>
//         <Grid item xs={12} md={3}>
//           <span>{order.fk_feed_type__type}</span>
//         </Grid>
//         <Grid item xs={12} md={3}>
//           <span>{order.total_request}</span>
//         </Grid>
//         <Grid item xs={12} md={3}>
//           <OrderProgress percentage={percentage} color={order.color} />
//         </Grid>
//       </Grid>
//     </div>
//   );
// };

// export default OrderDetails;

import { Grid } from '@mui/material';
import React from 'react';
import OrderProgress from './OrderProgress';

const OrderDetails = ({ order, index }) => {
  const percentage = order.total_request > 0 ? Math.floor((order.sum_delivery_quantity / order.total_request) * 100) : 0;

  const getColorForIndex = (index) => {
    const colors = ['#71dd37', '#248ecc', '#03c3ec', '#ffab00', '#ff3e1d', '#696cff', '#2ee0ca'];
    return colors[index % colors.length];
  };

  return (
    <div className="orders__details">
      <Grid container spacing={0}>
        <Grid item xs={12} md={3}>
          <span>{order.fk_to_broilers_farm__name || order.fk_to_breeders_farm__name}</span>
        </Grid>
        <Grid item xs={12} md={3}>
          <span>{order.fk_feed_type__type}</span>
        </Grid>
        <Grid item xs={12} md={3}>
          <span>{order.total_request}</span>
        </Grid>
        <Grid item xs={12} md={3}>
          <OrderProgress percentage={percentage} color={getColorForIndex(index)} />
        </Grid>
      </Grid>
    </div>
  );
};

export default OrderDetails;
