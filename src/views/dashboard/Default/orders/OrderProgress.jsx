import React from 'react';
import { Progress } from 'antd';

const OrderProgress = ({ percentage, color }) => {
  // const twoColors = {
  //   '0%': color,
  //   '100%': '#87d068'
  // };
  return <Progress style={{ marginBottom: '0px' }} percent={percentage} status="active" strokeColor={color} size={[270, 12]} />;
};

export default OrderProgress;
