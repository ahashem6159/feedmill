// import { Grid } from '@mui/material';
// import React from 'react';
// import OrderDetails from './OrderDetails';

// const Orders = ({ value, filteredData }) => {
//   const addColorToFilterBreeders = filteredData?.map((item, index) => ({
//     ...item,
//     color: getColorForIndex(index)
//   }));
//   const addColorToFilterBroilers = filteredData?.map((item, index) => ({
//     ...item,
//     color: getColorForIndex(index)
//   }));

//   function getColorForIndex(index) {
//     const colors = ['#71dd37', '#248ecc', '#03c3ec', '#ffab00', '#ff3e1d', '#696cff', '#2ee0ca']; // Add more colors as needed
//     return colors[index % colors.length];
//   }

//   return (
//     <div className="orders">
//       <div className="orders__title">
//         <Grid container spacing={0}>
//           <Grid item xs={12} md={3}>
//             <h5>FARM</h5>
//           </Grid>
//           <Grid item xs={12} md={3}>
//             <h5>FEED TYPE</h5>
//           </Grid>
//           <Grid item xs={12} md={3}>
//             <h5>REQ. QTY </h5>
//           </Grid>
//           <Grid item xs={12} md={3}>
//             <h5>DELIV. PERCENTAGE</h5>
//           </Grid>
//         </Grid>
//       </div>

//       {value === '1' && addColorToFilterBreeders?.map((order) => <OrderDetails order={order} key={order.fk_to_broilers_farm__name} />)}
//       {value === '2' && addColorToFilterBroilers?.map((order) => <OrderDetails order={order} key={order.fk_to_broilers_farm__name} />)}

//       {/* {orderData.length > 0 && orderData.map((order) => <OrderDetails order={order} key={order.farm} />)} */}
//     </div>
//   );
// };

// export default Orders;

// import { Grid } from '@mui/material';
// import React from 'react';
// import OrderDetails from './OrderDetails';

// const Orders = ({ filteredData }) => {
//   const ordersWithColor = filteredData?.map((item, index) => ({
//     ...item,
//     color: getColorForIndex(index)
//   }));

//   function getColorForIndex(index) {
//     const colors = ['#71dd37', '#248ecc', '#03c3ec', '#ffab00', '#ff3e1d', '#696cff', '#2ee0ca']; // Add more colors as needed
//     return colors[index % colors.length];
//   }

//   return (
//     <div className="orders">
//       <div className="orders__title">
//         <Grid container spacing={0}>
//           <Grid item xs={12} md={3}>
//             <h5>FARM</h5>
//           </Grid>
//           <Grid item xs={12} md={3}>
//             <h5>FEED TYPE</h5>
//           </Grid>
//           <Grid item xs={12} md={3}>
//             <h5>REQ. QTY</h5>
//           </Grid>
//           <Grid item xs={12} md={3}>
//             <h5>DELIV. PERCENTAGE</h5>
//           </Grid>
//         </Grid>
//       </div>
//       {ordersWithColor?.map((order) => (
//         <OrderDetails order={order} key={order.id} />
//       ))}{' '}
//       {/* Make sure 'id' is a unique identifier */}
//     </div>
//   );
// };

// export default Orders;

import { Grid } from '@mui/material';
import React from 'react';
import OrderDetails from './OrderDetails';

const Orders = ({ filteredData }) => {
  return (
    <div className="orders">
      <div className="orders__title">
        <Grid container spacing={0}>
          <Grid item xs={12} md={3}>
            <h5>FARM</h5>
          </Grid>
          <Grid item xs={12} md={3}>
            <h5>FEED TYPE</h5>
          </Grid>
          <Grid item xs={12} md={3}>
            <h5>REQ. QTY</h5>
          </Grid>
          <Grid item xs={12} md={3}>
            <h5>DELIV. PERCENTAGE</h5>
          </Grid>
        </Grid>
      </div>
      {filteredData.map((order, index) => (
        <OrderDetails order={order} index={index} key={order.id} />
      ))}
    </div>
  );
};

export default Orders;
