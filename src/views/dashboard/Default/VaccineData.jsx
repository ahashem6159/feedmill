import { Grid } from '@mui/material';
import React from 'react';

const VaccineData = ({ test }) => {
  console.log(test);

  let test12 = test === '1' || test === '2';
  let test13 = test === '1' || test === '3' || test === '4';
  let test4 = test === '4';
  return (
    <Grid item md={8} style={{ display: 'flex', alignItems: 'center', paddingRight: '18px' }}>
      <div className="assignCard__VaccineCard">
        <Grid container spacing={2} style={{ display: 'flex', flexWrap: 'noWrap' }}>
          {test13 && (
            <Grid item md={3}>
              <div className="vaccineData">
                <div className="vaccineData__content">
                  <p>Live</p>
                  <p>Cevac</p>
                </div>
                <div className="vaccineData__content">
                  <p>2500</p>
                  <p>2000</p>
                </div>
                <div className="vaccineData__content">
                  <p>12/1/2022</p>
                  <p>21/2/2022</p>
                </div>
              </div>
            </Grid>
          )}
          {test12 && (
            <Grid item md={3}>
              <div className="vaccineData">
                <div className="vaccineData__content">
                  <p>Live</p>
                  <p>Cevac</p>
                </div>
                <div className="vaccineData__content">
                  <p>2500</p>
                  <p>2000</p>
                </div>
                <div className="vaccineData__content">
                  <p>12/1/2022</p>
                  <p>21/2/2022</p>
                </div>
              </div>
            </Grid>
          )}
          {test13 && (
            <Grid item md={3}>
              <div className="vaccineData">
                <div className="vaccineData__content">
                  <p>Live</p>
                  <p>Cevac</p>
                </div>
                <div className="vaccineData__content">
                  <p>2500</p>
                  <p>2000</p>
                </div>
                <div className="vaccineData__content">
                  <p>12/1/2022</p>
                  <p>21/2/2022</p>
                </div>
              </div>
            </Grid>
          )}
          {test4 && (
            <Grid item md={3}>
              <div className="vaccineData">
                <div className="vaccineData__content">
                  <p>Live</p>
                  <p>Cevac</p>
                </div>
                <div className="vaccineData__content">
                  <p>2500</p>
                  <p>2000</p>
                </div>
                <div className="vaccineData__content">
                  <p>12/1/2022</p>
                  <p>21/2/2022</p>
                </div>
              </div>
            </Grid>
          )}
          <Grid item md={3}>
            <div className="vaccineData">
              <div className="vaccineData__content">
                <p>Live</p>
                <p>Cevac</p>
              </div>
              <div className="vaccineData__content">
                <p>2500</p>
                <p>2000</p>
              </div>
              <div className="vaccineData__content">
                <p>12/1/2022</p>
                <p>21/2/2022</p>
              </div>
            </div>
          </Grid>
          <Grid item md={3}>
            <div className="vaccineData">
              <div className="vaccineData__content">
                <p>Live</p>
                <p>Cevac</p>
              </div>
              <div className="vaccineData__content">
                <p>2500</p>
                <p>2000</p>
              </div>
              <div className="vaccineData__content">
                <p>12/1/2022</p>
                <p>21/2/2022</p>
              </div>
            </div>
          </Grid>
          <Grid item md={3}>
            <div className="vaccineData">
              <div className="vaccineData__content">
                <p>Live</p>
                <p>Cevac</p>
              </div>
              <div className="vaccineData__content">
                <p>2500</p>
                <p>2000</p>
              </div>
              <div className="vaccineData__content">
                <p>12/1/2022</p>
                <p>21/2/2022</p>
              </div>
            </div>
          </Grid>
          <Grid item md={3}>
            <div className="vaccineData">
              <div className="vaccineData__content">
                <p>Live</p>
                <p>Cevac</p>
              </div>
              <div className="vaccineData__content">
                <p>2500</p>
                <p>2000</p>
              </div>
              <div className="vaccineData__content">
                <p>12/1/2022</p>
                <p>21/2/2022</p>
              </div>
            </div>
          </Grid>
          <Grid item md={3}>
            <div className="vaccineData">
              <div className="vaccineData__content">
                <p>Live</p>
                <p>Cevac</p>
              </div>
              <div className="vaccineData__content">
                <p>2500</p>
                <p>2000</p>
              </div>
              <div className="vaccineData__content">
                <p>12/1/2022</p>
                <p>21/2/2022</p>
              </div>
            </div>
          </Grid>
        </Grid>
      </div>
    </Grid>
  );
};

export default VaccineData;
