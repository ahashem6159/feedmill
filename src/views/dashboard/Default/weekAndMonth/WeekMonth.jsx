import { Grid } from '@mui/material';
import React from 'react';
import ApexCharts from 'react-apexcharts';
import AreaChart from './AreaChart';
import './area.css';

const WeekMonth = ({ weeklyFeed, totalWorkingHoursPerDay }) => {
  // const quantity = weeklyFeed?.map((q) => Math.floor(q.sum_quantity)).slice(-7);
  const quantity = weeklyFeed?.map((q) => q.sum_quantity.toFixed(3)).slice(-7);
  const sumQunatity = weeklyFeed?.slice(-7).reduce((acc, cur) => acc + cur.sum_quantity, 0);
  // const totalDay = Object.keys(totalWorkingHoursPerDay).reduce((acc, cur) => acc + new Date(cur).getDate(), 0);
  const days = weeklyFeed?.slice(-7).map((day) => new Date(day.the_date).toLocaleDateString('en-US', { weekday: 'short' }));
  let isNotTotalWorkingHoursPerDay = Object.keys(totalWorkingHoursPerDay).length === 0;
  const isAllEmpty = quantity?.map((el) => Math.floor(el)).every((el) => el === 0);

  let totalDay = 0;

  for (let key in totalWorkingHoursPerDay) {
    if (Object.prototype.hasOwnProperty.call(totalWorkingHoursPerDay, key)) {
      totalDay += totalWorkingHoursPerDay[key];
    }
  }

  const series = [
    {
      // name: 'Ton/Hour',
      data: quantity // Example data for each quantity
    }
  ];

  const options = {
    chart: {
      type: 'bar',
      height: 350,
      width: '30%',
      toolbar: {
        show: false // This hides the toolbar
      },
      sparkline: {
        enabled: false // This hides the borders
      }
    },
    plotOptions: {
      bar: {
        borderRadius: 8,
        horizontal: false,
        columnWidth: '55%',
        distributed: true // This will give the individual color to each bar if colors are provided in an array
      }
    },
    dataLabels: {
      enabled: false
    },
    xaxis: {
      categories: days, // Labels for the days of the week
      labels: {
        style: {
          colors: Array(7).fill('#7f82ff'), // Colors for the labels
          fontSize: '14px'
        }
      },
      axisBorder: {
        show: false // This hides the x-axis border
      }
    },
    yaxis: {
      show: false, // Hide y-axis
      min: 0 // Custom minimum value for the y-axis
      //   max: 100
    },
    grid: {
      show: false // Hide grid lines
    },
    tooltip: {
      enabled: true,
      theme: 'dark',
      x: { format: 'dd/MM/yy HH:mm' },
      y: {
        formatter: function (val) {
          return val + ' Tons'; // Custom text for the tooltip
        }
      }
    },
    legend: {
      show: false // This hides the legend
    }
  };

  return (
    <Grid container spacing={2}>
      {isAllEmpty ? (
        <Grid item xs={12} md={12}>
          <div className="productionCard1">
            <div className="card__date">
              <h4>Weekly Feed Production</h4>
              <span>Last Week</span>
            </div>
            <div className="noDataContainer">
              <p>No Productions Data</p>
            </div>
          </div>
        </Grid>
      ) : (
        <Grid item xs={12} md={12}>
          <div className="card" style={{ paddingBottom: '7px' }}>
            <div className="card__date">
              <h4>Weekly Feed Production</h4>
              <span>Last Week</span>
            </div>
            <div className="card__content">
              <Grid container spacing={0} sx={{ display: 'flex', alignItems: 'center' }}>
                <Grid item xs={12} md={3}>
                  <div className="card__content__numbers">
                    <span className="card__content__numbers--span1">
                      {/* {Math.floor(sumQunatity)} <span style={{ fontSize: '13px' }}>Tons</span> */}
                      {sumQunatity.toFixed(3)} <span style={{ fontSize: '13px' }}>Tons</span>
                    </span>
                  </div>
                </Grid>
                <Grid item xs={12} md={9}>
                  <div id="chart">
                    <ApexCharts options={options} series={series} type="bar" height={180} />
                  </div>
                </Grid>
              </Grid>
            </div>
          </div>
        </Grid>
      )}
      {isNotTotalWorkingHoursPerDay ? (
        <Grid item xs={12} md={12}>
          <div className="productionCard1">
            <div className="card__date">
              <h4>Month-to-Date</h4>
              <span>Working Hours</span>
            </div>
            <div className="noDataContainer">
              <p>Sorry, No Data</p>
            </div>
          </div>
        </Grid>
      ) : (
        <Grid item xs={12} md={12}>
          <div className="card" style={{ paddingBottom: '7px' }}>
            <div className="card__date">
              <h4>Month-to-Date</h4>
              <span>Working Hours</span>
            </div>
            <div className="card__content">
              <Grid container spacing={0} sx={{ display: 'flex', alignItems: 'center' }}>
                <Grid item xs={12} md={3}>
                  <div className="card__content__numbers">
                    <span className="card__content__numbers--span1">
                      {totalDay.toFixed(3)}
                      <span style={{ fontSize: '13px' }}>Hours</span>
                    </span>
                    {/* <span className="card__content__numbers--span2" style={{ color: '#7bdf45' }}>
                    <ArrowUpwardIcon className="iconUp" />
                    24.8%
                  </span> */}
                  </div>
                </Grid>
                <Grid item xs={12} md={9}>
                  <div id="chart">
                    {/* <ApexCharts options={options} series={series} type="bar" height={180} /> */}
                    <AreaChart totalWorkingHoursPerDay={totalWorkingHoursPerDay} />
                  </div>
                </Grid>
              </Grid>
            </div>
          </div>
        </Grid>
      )}
    </Grid>
  );
};

export default WeekMonth;
