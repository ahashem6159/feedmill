import React from 'react';
import ReactApexChart from 'react-apexcharts';
// import noDataImg from '../../../../assets/images/dataImg.jpg';

const AreaChart = ({ totalWorkingHoursPerDay }) => {
  if (!totalWorkingHoursPerDay) {
    return <div>Loading...</div>;
  }

  const day = Object.keys(totalWorkingHoursPerDay).map((el) => new Date(el).getDate());
  // const time = Object.values(totalWorkingHoursPerDay).map((el) => Math.floor(el));
  const time = Object.values(totalWorkingHoursPerDay).map((el) => el.toFixed(3));

  const series = [
    {
      name: 'Working Hour',
      data: time // Sample data, replace with your actual data
      // data: [31, 40, 28, 51, 42, 109, 100, 50, 65] // Sample data, replace with your actual data
    }
  ];

  const options = {
    chart: {
      type: 'area',
      toolbar: {
        show: false
      }
    },
    dataLabels: {
      enabled: false
    },
    stroke: {
      curve: 'smooth',
      width: 2
    },
    colors: ['#00E396'], // Green color
    fill: {
      type: 'gradient',
      gradient: {
        shadeIntensity: 1,
        opacityFrom: 0.3,
        opacityTo: 0.4,
        stops: [0, 90, 100]
      }
    },
    xaxis: {
      categories: day,
      axisBorder: {
        show: false // This hides the x-axis border
      },
      axisTicks: {
        show: false
      },
      tickAmount: 13
    },
    tooltip: {
      enabled: true,
      theme: 'dark',
      x: { format: 'dd/MM/yy HH:mm' }
    },
    grid: {
      show: true,
      borderColor: '#90A4AE',
      strokeDashArray: 0,
      position: 'back',
      xaxis: {
        lines: {
          show: false
        }
      },
      yaxis: {
        lines: {
          show: false
        }
      },
      row: {
        colors: undefined,
        opacity: 0.5
      },
      column: {
        colors: undefined,
        opacity: 0.5
      },
      padding: {
        top: 0,
        right: 0,
        bottom: 0,
        left: 0
      }
    },
    yaxis: {
      labels: {
        formatter: (value) => `${value.toFixed(0)}` // No decimals
      }
    },
    legend: {
      show: false
    }
  };

  // if (Object.keys(totalWorkingHoursPerDay).length === 0) return <img className="noDataImg" src={noDataImg} alt="" />;

  return (
    <div id="chart">
      <ReactApexChart options={options} series={series} type="area" height={205} />
    </div>
  );
};

export default AreaChart;
