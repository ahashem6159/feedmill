import React from 'react';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';

const BarChart = () => {
  const chartConfig = {
    chart: {
      type: 'bar',
      height: 442,
      spacingTop: 20,
      spacingRight: 20,
      spacingBottom: 20,
      spacingLeft: 20
    },
    title: {
      text: 'Last 7 Day Production',
      align: 'left'
      // margin: 10
    },
    xAxis: {
      categories: ['2-Dec', '3-Dec', '4-Dec', '5-Dec'],
      title: {
        text: null
      },
      gridLineWidth: 1,
      lineWidth: 0
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Hours / Tons',
        align: 'high'
      },
      labels: {
        overflow: 'justify'
      },
      gridLineWidth: 0
    },
    tooltip: {
      // valueSuffix: ' Hours'
      formatter: function () {
        return (
          '<b>' +
          this.x +
          '</b><br>' +
          this.series.name +
          ': ' +
          this.y +
          '<b> ' +
          (this.series.name === 'Deilivered Ton/Hour' ? 'Tons' : 'hours')
        );
      },
      backgroundColor: 'rgba(255, 255, 255, 0.85',
      borderColor: '#ddd',
      borderRadius: 5,
      borderWidth: 1,
      shadow: true
    },
    plotOptions: {
      bar: {
        borderRadius: '50%',
        dataLabels: {
          enabled: true
        },
        groupPadding: 0.1
      }
    },
    legend: {
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'top',
      x: -40,
      y: 80,
      floating: true,
      borderWidth: 1,
      backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
      shadow: true
    },
    credits: {
      enabled: false
    },
    series: [
      {
        name: 'Working Hours',
        data: [8, 5, 8, 6],
        color: '#248ecc'
      },
      {
        name: 'Downtime Hours',
        data: [1, 3, 9, 2],
        color: '#ffac5e'
      },
      {
        name: 'Deilivered Ton/Hour',
        data: [6, 5, 15, 3],
        color: '#08e499'
      }
    ],

    responsive: {
      rules: [
        {
          condition: {
            maxWidth: 500
          },
          chartOptions: {
            legend: {
              layout: 'horizontal',
              align: 'center',
              verticalAlign: 'bottom'
            }
          }
        }
      ]
    }
  };

  return (
    <div style={{ borderRadius: '10px', overflow: 'hidden' }}>
      <HighchartsReact highcharts={Highcharts} options={chartConfig} />
    </div>
  );
};

export default BarChart;
