import React from 'react';
import Chart from 'react-apexcharts';
import Tons from '../totalTons/Tons';
// import CustomIconButton from 'common/CustomIconButton';
import { Button } from '@mui/material';
import { useState } from 'react';

const DonutChart = ({ productions, delivery_information }) => {
  const [isDelivery, setIsDelivery] = useState(1);

  const colors = ['#ff6178', '#696cff', '#ffab00', '#03c3ec', '#71dd37', '#8592a3', '#a1acb8'];

  const productionName = productions?.map((day) => day.fk_formula__fk_feed_type__type);
  const sumQunatity = productions?.reduce((acc, cur) => acc + cur.sum_quantity, 0);
  const percentage = productions?.map((day) => Math.floor(day.percentage * 100));
  const series = percentage;

  const deliveryName = delivery_information?.map((day) => day.fk_feed_type__type);
  const sumQunatityDelivery = delivery_information?.reduce((acc, cur) => acc + cur.sum_delivery_quantity, 0);
  // const seriesDelivery = delivery_information?.map((day) => Number(day.sum_delivery_quantity.toFixed(2)));
  const seriesDelivery = delivery_information?.map((day) => Number(((day.sum_delivery_quantity / sumQunatityDelivery) * 100).toFixed(2)));

  const handleButtonClick = (id) => {
    setIsDelivery(id);
  };

  if (productions?.length <= 0 && isDelivery === 1)
    return (
      <div className="productionCard">
        <div className="card__date" style={{ display: 'flex', justifyContent: 'flex-start' }}>
          <Button variant={isDelivery === 1 && 'contained'} onClick={() => handleButtonClick(1)} style={{ marginRight: '10px' }}>
            {/* {isDelivery ? 'Delivery' : 'Productions'} */}
            Productions
          </Button>
          <Button variant={isDelivery === 2 && 'contained'} onClick={() => handleButtonClick(2)}>
            {/* {isDelivery ? 'Delivery' : 'Productions'} */}
            Delivery
          </Button>
        </div>
        <div className="noDataContainer">
          <p>No Productions Data</p>
        </div>
      </div>
    );

  if (delivery_information?.length <= 0 || !delivery_information)
    return (
      <div className="productionCard">
        <div className="card__date" style={{ display: 'flex', justifyContent: 'flex-start' }}>
          <Button variant={isDelivery === 1 && 'contained'} onClick={() => handleButtonClick(1)} style={{ marginRight: '10px' }}>
            {/* {isDelivery ? 'Delivery' : 'Productions'} */}
            Productions
          </Button>
          <Button variant={isDelivery === 2 && 'contained'} onClick={() => handleButtonClick(2)}>
            {/* {isDelivery ? 'Delivery' : 'Productions'} */}
            Delivery
          </Button>
        </div>
        <div className="noDataContainer">
          <p>No Deliveries Data</p>
        </div>
      </div>
    );

  return (
    <div className="donutCard">
      <div className="donutContainer">
        <div className="donutContainer1" style={{ display: 'flex', justifyContent: 'flex-start' }}>
          {/* <span className="donutContainer1--h1">{isDelivery ? 'Production' : ' Delivery'}</span> */}
          {/* <CustomIconButton className="donutContainer1__icon" /> */}
          <Button variant={isDelivery === 1 && 'contained'} onClick={() => handleButtonClick(1)}>
            {/* {isDelivery ? 'Delivery' : 'Productions'} */}
            Productions
          </Button>
          <Button variant={isDelivery === 2 && 'contained'} onClick={() => handleButtonClick(2)}>
            {/* {isDelivery ? 'Delivery' : 'Productions'} */}
            Delivery
          </Button>
        </div>
        {isDelivery === 1 && (
          <div className="bonutChart">
            <div className="production">
              {/* <span className="production--span1">{Math.floor(sumQunatity)}</span> */}
              <span className="production--span1">{sumQunatity.toFixed(3)}</span>
              <span className="production--span2">TONS</span>
            </div>
            <Chart
              type="donut"
              width={150}
              height={150}
              series={series}
              options={{
                colors: colors,
                stroke: {
                  width: 5
                },
                labels: productionName,
                // title: {
                //   text: 'Production Percentage %',
                //   align: 'center',
                //   style: { fontSize: '13px', fontWeight: '500' }
                // },
                plotOptions: {
                  pie: {
                    customScale: 1.1,
                    expandOnClick: true,
                    donut: {
                      size: '76%',
                      labels: {
                        show: true,
                        name: {
                          fontSize: '10px', // Adjust the font size for the name label
                          fontWeight: '600',
                          offsetY: 23
                        },
                        value: {
                          fontSize: '20px',
                          fontWeight: '100', // Adjust the font size for the value label
                          color: '#666',
                          offsetY: -8
                        },
                        total: {
                          show: true,
                          fontSize: '0.7125rem',
                          color: '#696cff',
                          label: '',
                          formatter: function () {
                            return percentage[1];
                          }
                        }
                      }
                    }
                  }
                },
                dataLabels: {
                  enabled: false,
                  style: {
                    fontSize: 10 // Set font size for data labels
                  }
                },
                legend: {
                  show: false // Add this line to remove the legend
                },
                tooltip: {
                  style: {
                    fontFamily: 'Arial, sans-serif'
                  },
                  y: {
                    formatter: function (value) {
                      return value + '%'; // Add '%' symbol to the value
                    }
                  }
                }
              }}
            />
          </div>
        )}
        {isDelivery === 2 && (
          <div className="bonutChart">
            <div className="production">
              {/* <span className="production--span1">{Math.floor(sumQunatityDelivery)}</span> */}
              <span className="production--span1">{sumQunatityDelivery.toFixed(3)}</span>
              <span className="production--span2">TONS</span>
            </div>
            <Chart
              type="donut"
              width={150}
              height={150}
              series={seriesDelivery}
              options={{
                colors: colors,
                stroke: {
                  width: 5
                },
                labels: deliveryName,
                // title: {
                //   text: 'Production Percentage %',
                //   align: 'center',
                //   style: { fontSize: '13px', fontWeight: '500' }
                // },
                plotOptions: {
                  pie: {
                    customScale: 1.1,
                    expandOnClick: true,
                    donut: {
                      size: '76%',
                      labels: {
                        show: true,
                        name: {
                          fontSize: '10px', // Adjust the font size for the name label
                          fontWeight: '600',
                          offsetY: 23
                        },
                        value: {
                          fontSize: '20px',
                          fontWeight: '100', // Adjust the font size for the value label
                          color: '#666',
                          offsetY: -8
                        },
                        total: {
                          show: true,
                          fontSize: '0.7125rem',
                          color: '#696cff',
                          label: '',
                          formatter: function () {
                            return Number(seriesDelivery[1]);
                          }
                        }
                      }
                    }
                  }
                },
                dataLabels: {
                  enabled: false,
                  style: {
                    fontSize: 10 // Set font size for data labels
                  }
                },
                legend: {
                  show: false // Add this line to remove the legend
                },
                tooltip: {
                  style: {
                    fontFamily: 'Arial, sans-serif'
                  },
                  y: {
                    formatter: function (value) {
                      return value + '%'; // Add '%' symbol to the value
                    }
                  }
                }
              }}
            />
          </div>
        )}
      </div>
      {isDelivery === 1 && <Tons productions={productions} />}
      {isDelivery === 2 && <Tons delivery_information={delivery_information} />}
    </div>
  );
};

export default DonutChart;
