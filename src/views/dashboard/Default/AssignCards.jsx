import { Button, Grid } from '@mui/material';
import CustomSelectSearch from 'common/CustomSelectSearch';
import React from 'react';
import { useState } from 'react';
import VaccineCard from './VaccineCard';
import VaccineData from './VaccineData';

const top100Films = ['Machine 1', 'Machine 2', 'Machine 3', 'Machine 4'];

const AssignCards = ({ test, img }) => {
  const [selectedValue, setSelectedValue] = useState(top100Films[0]);
  const [isOpenSelectOption, setIsOpenSelectOption] = useState(false);
  const [isOpenFormVac, setIsOpenFormVac] = useState(false);

  const handleAutocompleteChange = (event, newValue) => {
    setSelectedValue(newValue);
    setIsOpenSelectOption(false);
  };

  return (
    <div className="assignCard">
      <Grid container spacing={0}>
        <Grid item md={2} style={{ paddingTop: '0px' }}>
          <div className="assignCard__assignName">
            <div className="assignCard__assignName--content">
              <h3>
                <img src={img} alt="" /> Hossam Hashem
              </h3>
              <span>{selectedValue}</span>
              <button onClick={() => setIsOpenSelectOption(!isOpenSelectOption)}>Assign {isOpenSelectOption ? '-' : '+'}</button>
            </div>
            {isOpenSelectOption && (
              <div className="assignBtn">
                <CustomSelectSearch
                  isAssignClasses="assignClasses"
                  data={top100Films}
                  value={selectedValue}
                  onChange={handleAutocompleteChange}
                />
              </div>
            )}
          </div>
        </Grid>
        <Grid item md={2} style={{ paddingTop: '0px' }}>
          <div className="assignCard__addVac">
            <Button className="button-animate" onClick={() => setIsOpenFormVac(!isOpenFormVac)}>
              Add Vaccine +
            </Button>
          </div>
        </Grid>
        {/* <Grid item xs={8} style={{ display: 'flex', alignItems: 'center', paddingRight: '18px' }}> */}
        {isOpenFormVac ? <VaccineCard /> : <VaccineData test={test} />}
        {/* </Grid> */}
      </Grid>
    </div>
  );
};

export default AssignCards;
