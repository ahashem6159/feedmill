import PropTypes from 'prop-types';
import TotalIncomeCard from 'ui-component/cards/Skeleton/TotalIncomeCard';
import { Grid } from '@mui/material';
import { gridSpacing } from 'store/constant';

const TonsCard = ({ isLoading, title, price }) => {
  let firstChar;
  let colorName;

  let testPrice = price.toFixed(3);

  const charArr = title.split(' ');
  console.log(charArr);

  if (charArr.length > 1 && charArr.length === 4) {
    firstChar = charArr[0][0] + charArr[1][0];
    colorName = charArr[0] + charArr[1] + charArr[2];
  } else if (charArr.length > 1 && charArr.length === 3) {
    firstChar = charArr[0][0] + charArr[1][0];
    colorName = charArr[0] + charArr[1] + charArr[2];
  } else if (charArr.length > 1 && charArr.length === 2) {
    firstChar = charArr[0][0] + charArr[1][0];
    colorName = charArr[0] + charArr[1]
  } else {
    firstChar = charArr[0][0];
    colorName = charArr[0];
  }

  console.log(firstChar);
  console.log(colorName);

  console.log(title);

  return (
    <>
      {isLoading ? (
        <TotalIncomeCard />
      ) : (
        <Grid container spacing={gridSpacing}>
          <Grid item xs={12}>
            <Grid container spacing={gridSpacing}>
              <Grid item xs={12} md={10}>
                <div className="colorContainer">
                  {/* <span className={`color-${color}`}></span> */}
                  <span className={`colorContainer__color--${colorName}`}>{firstChar}</span>
                  <h6 className="tonTitle1">{title}</h6>
                </div>
              </Grid>
              <Grid item xs={12} md={2}>
                <div className="Details1">
                  {/* <span className="Details1__span">{Math.floor(price)}</span> */}
                  <span className="Details1__span">{testPrice}</span>
                  {/* <span className="Details__Precentage">{percentage}</span> */}
                </div>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      )}
    </>
  );
};

TonsCard.propTypes = {
  isLoading: PropTypes.bool
};

export default TonsCard;
