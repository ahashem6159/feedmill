import React from 'react';
import TonsCard from './TonsCard';
import { useState, useEffect, useRef } from 'react';
// import AgricultureIcon from '@mui/icons-material/Agriculture';

const Tons = ({ productions, delivery_information }) => {
  const [div1Height, setDiv1Height] = useState(0);
  const div1Ref = useRef(null);

  useEffect(() => {
    if (div1Ref.current) {
      setDiv1Height(div1Ref.current.clientHeight);
    }
  }, []);

  console.log(div1Height);

  let details;

  if (productions)
    details = (
      <div>
        {productions.map((pro) => (
          <TonsCard
            key={pro.fk_formula__fk_feed_type__id}
            color={pro.fk_formula__fk_feed_type__type}
            title={pro.fk_formula__fk_feed_type__type}
            price={pro.sum_quantity}
          />
        ))}
      </div>
    );

  if (delivery_information)
    details = (
      <div>
        {delivery_information.map((pro, index) => (
          <TonsCard key={index} color={pro.fk_feed_type__type} title={pro.fk_feed_type__type} price={pro.sum_delivery_quantity} />
        ))}
      </div>
    );

  return (
    <div ref={div1Ref} className={`${'tons'} ${delivery_information && 'tonsDelivery'}`}>
      {details}
    </div>
  );
};

export default Tons;
