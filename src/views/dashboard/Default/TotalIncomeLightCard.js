import { styled } from '@mui/material/styles';
import MainCard from 'ui-component/cards/MainCard';
import QueryBuilderIcon from '@mui/icons-material/QueryBuilder';
import LightModeIcon from '@mui/icons-material/LightMode';

const CardWrapper = styled(MainCard)(({ theme }) => ({
  overflow: 'hidden',
  position: 'relative',
  '&:after': {
    content: '""',
    position: 'absolute',
    width: 210,
    height: 210,
    background: `linear-gradient(210.04deg, ${theme.palette.warning.dark} -50.94%, rgba(144, 202, 249, 0) 83.49%)`,
    borderRadius: '50%',
    top: -30,
    right: -180
  },
  '&:before': {
    content: '""',
    position: 'absolute',
    width: 210,
    height: 210,
    background: `linear-gradient(140.9deg, ${theme.palette.warning.dark} -14.02%, rgba(144, 202, 249, 0) 70.50%)`,
    borderRadius: '50%',
    top: -160,
    right: -130
  }
}));

const TotalIncomeLightCard = () => {
  return (
    <CardWrapper border={false} content={false}>
      <div className="totalIncomeCard">
        <div className="totalIncomeCard__details__hours">
          <h5 className="totalIncomeCard__details__hours--hour">
            <QueryBuilderIcon className="hourIcon" /> Average Deilivery Ton/Hour
          </h5>
          <p>
            <span className="totalIncomeCard__details__hours--tons">20.6 Tons</span>
            <span>/Hr</span>
          </p>
        </div>
        <div className="totalIncomeCard__details__days">
          <h5 className="totalIncomeCard__details__hours--day">
            <LightModeIcon className="dayIcon" /> Average Deilivery Ton/Day
          </h5>
          <p>
            <span className="totalIncomeCard__details__hours--tons">20.6 Tons</span>
            <span>/Day</span>
          </p>
        </div>
      </div>
    </CardWrapper>
  );
};

export default TotalIncomeLightCard;
