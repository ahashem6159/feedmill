import MaterialTable from 'material-table';
import React from 'react';

// import GetAppIcon from '@material-ui/icons/GetApp';
// import AddIcon from '@material-ui/icons/Add';
// import { useState } from 'react';

const TestTable = () => {
  const columns = [
    { title: 'Name', field: 'name' },
    { title: 'Email', field: 'email' }
  ];

  return <MaterialTable columns={columns} />;
};

export default TestTable;
