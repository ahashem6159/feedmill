import React from 'react';
// import { Link } from 'react-router-dom';
import { DataGrid } from '@mui/x-data-grid';
import { TextField } from '@mui/material';
// import Button from '@mui/material/Button';

const columns = [
  { field: 'id', headerName: 'ID', width: 70 },
  { field: 'materialName', headerName: 'Material Name', width: 130 },
  { field: 'itemCode', headerName: 'Item Code', width: 130 },
  {
    field: 'openStockInKgs',
    headerName: 'Open Stock In KGS',
    // type: 'number',
    width: 160
  },
  {
    field: 'quantityConsumedInKGs',
    headerName: 'Quantity Consumed in KGs',
    description: 'This column has a value getter and is not sortable.',
    sortable: false,
    width: 170
    // valueGetter: (params) => `${params.row.firstName || ''} ${params.row.lastName || ''}`
  },
  {
    field: 'addedValueInKGs',
    headerName: 'Added Value in KGs',
    description: 'This column has a value getter and is not sortable.',
    sortable: false,
    width: 160
    // renderCell: () => <input type="text" style={{ width: '100%', outline: 'none' }} value={0} />
  }
];

const rows = [
  { id: 1, materialName: 'Snow', itemCode: 'PRB', openStockInKgs: 35, quantityConsumedInKGs: 3009.14, addedValueInKGs: 22 },
  { id: 2, materialName: 'Lannister', itemCode: 'ISO', openStockInKgs: 42, quantityConsumedInKGs: 1806.45, addedValueInKGs: 31 },
  { id: 3, materialName: 'Lannister', itemCode: 'PHY', openStockInKgs: 45, quantityConsumedInKGs: 420.79, addedValueInKGs: 13 },
  { id: 4, materialName: 'Stark', itemCode: 'POZ', openStockInKgs: 16, quantityConsumedInKGs: 2168.23, addedValueInKGs: 73 },
  { id: 5, materialName: 'Targaryen', itemCode: 'ARG', openStockInKgs: 20, quantityConsumedInKGs: 130.13, addedValueInKGs: 93 },
  { id: 6, materialName: 'Melisandre', itemCode: '110253', openStockInKgs: 150, quantityConsumedInKGs: 20638.25, addedValueInKGs: 10 },
  { id: 7, materialName: 'Clifford', itemCode: '110123', openStockInKgs: 44, quantityConsumedInKGs: 0.0, addedValueInKGs: 83 },
  { id: 8, materialName: 'Frances', itemCode: '110189', openStockInKgs: 36, quantityConsumedInKGs: 6923.73, addedValueInKGs: 40 },
  { id: 9, materialName: 'Roxie', itemCode: '110145', openStockInKgs: 65, quantityConsumedInKGs: 0.0, addedValueInKGs: 51 }
];

const MaterialStock = () => {
  return (
    <div className="materialStock">
      <div className="materialStock__content">
        <h2 className="materialStock__h2">Material Stock</h2>
        <span className="formula__link">
          <TextField id="standard-basic" label="Search" variant="outlined" />
        </span>
      </div>
      <div style={{ height: 400, width: '100%' }}>
        <DataGrid
          style={{ background: '#fff' }}
          rows={rows}
          columns={columns}
          initialState={{
            pagination: {
              paginationModel: { page: 0, pageSize: 5 }
            }
          }}
          pageSizeOptions={[5, 10]}
        />
      </div>
    </div>
  );
};

export default MaterialStock;
