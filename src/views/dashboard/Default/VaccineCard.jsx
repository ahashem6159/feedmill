import { Button, Grid } from '@mui/material';
import CustomInput from 'common/CustomInput';
import CustomSelectSearch from 'common/CustomSelectSearch';
import CustomTimePicker from 'common/CustomTimePicker';
import React from 'react';
import { useState } from 'react';

const top100Films = ['The Shawshank Redemption', 'The Dark Knight', 'The Godfather'];

const VaccineCard = () => {
  const [selectedValue, setSelectedValue] = useState(null);

  const handleAutocompleteChange = (event, newValue) => {
    setSelectedValue(newValue);
  };

  return (
    <Grid item md={8} style={{ display: 'flex', alignItems: 'center', paddingRight: '18px' }}>
      <div className="vaccineCardContainer">
        <div className="assignCard__VaccineCardForm">
          <CustomSelectSearch title="Vaccine Category" data={top100Films} value={selectedValue} onChange={handleAutocompleteChange} />
          <CustomSelectSearch title="Vaccine Name" data={top100Films} value={selectedValue} onChange={handleAutocompleteChange} />
          <CustomInput title="Dose" />
          <CustomInput title="Cal." />
          <CustomTimePicker title="Start" />
          <CustomTimePicker title="End" />
        </div>
        <div className="assignCard__VaccineCard1">
          <Button variant="outlined">Save</Button>
          <Button variant="outlined">Cancel</Button>
        </div>
      </div>
    </Grid>
  );
};

export default VaccineCard;
