import React from 'react';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';

const PieChart = ({ setNameChart }) => {
  const chartConfig = {
    chart: {
      type: 'pie',
      height: '475px'
    },
    title: {
      text: 'Production Percentage %'
    },
    tooltip: {
      valueSuffix: '%'
    },
    plotOptions: {
      series: {
        allowPointSelect: true,
        cursor: 'pointer',
        point: {
          events: {
            click: function () {
              setNameChart(this.name);
            }
          }
        },
        showInLegend: true,
        dataLabels: [
          {
            enabled: true,
            distance: 20
          },
          {
            enabled: true,
            distance: -40,
            format: '{point.percentage:.1f}%',
            style: {
              fontSize: '1.2em',
              textOutline: 'none',
              opacity: 0.7
            },
            filter: {
              operator: '>',
              property: 'percentage',
              value: 10
            }
          }
        ]
      }
    },
    series: [
      {
        name: 'Percentage',
        colorByPoint: true,
        data: [
          {
            name: 'Starter',
            y: 40.02
          },
          {
            name: 'Grower',
            // sliced: true,
            // selected: true,
            y: 26.71
          },
          {
            name: 'Finisher',
            y: 1.09
          },
          {
            name: 'Broiler Starter',
            y: 15.5
          },
          {
            name: 'Breeder Starter Mash',
            y: 1.68
          },
          {
            name: 'Breeder Grower Mash',
            y: 4.68
          },
          {
            name: 'Breeder Production 01 Mash',
            y: 3.68
          },
          {
            name: 'Breeder Production 02 Mash',
            y: 3.0
          },
          {
            name: 'Breeder Male Mash',
            y: 3.0
          }
        ]
      }
    ],

    responsive: {
      rules: [
        {
          condition: {
            maxWidth: 500
          },
          chartOptions: {
            legend: {
              layout: 'horizontal',
              align: 'center',
              verticalAlign: 'bottom'
            }
          }
        }
      ]
    }
  };

  // useEffect(() => {
  // const colors = Highcharts.getOptions().colors;

  // }, []);

  return (
    <div>
      <HighchartsReact highcharts={Highcharts} options={chartConfig} />
    </div>
  );
};

export default PieChart;
