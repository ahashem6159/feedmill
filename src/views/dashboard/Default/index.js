import React, { useEffect } from 'react';
import { Grid } from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation } from 'react-router';

import { fetchDataDashboard } from 'slices/dashboardSlice';
import { gridSpacing } from 'store/constant';
import CustomLoading from 'common/CustomLoading';
import PageNotFound from 'views/pages/authentication/PageNotFound';
// import DonutChart from './donutChart/DonutChart';
// import WeekMonth from './weekAndMonth/WeekMonth';
// import CopyTotalIncome from './copyTotalIncomeCards/CopyTotalIncome';
// import { MuiTabs } from './MuiTabs';
import AssignCards from './AssignCards';

import img1 from '../../../assets/images/2.png';
import img2 from '../../../assets/images/3.png';
import img3 from '../../../assets/images/4.png';

const isError = (error) => {
  return error && typeof error === 'string' && ['404', '500', 'Network Error'].some((err) => error.includes(err));
};

const Dashboard = () => {
  const dispatch = useDispatch();
  const location = useLocation();
  const { dataDashboard, isLoading, error } = useSelector((state) => state.dashboard);

  // Fetch dashboard data
  useEffect(() => {
    dispatch(fetchDataDashboard());
  }, [dispatch, location.pathname]);

  if (isLoading || !dataDashboard) {
    return <CustomLoading />;
  }

  if (isError(error)) {
    return <PageNotFound />;
  }

  // const {
  //   weekly_feed,
  //   productions,
  //   total_production_hours,
  //   total_down_times,
  //   total_working_hours_per_day,
  //   delivery_info,
  //   total_time_working,
  //   total_delivery_last_month_by_hour,
  //   delivery_information
  // } = dataDashboard;

  return (
    <Grid container spacing={gridSpacing}>
      {/* <Grid item xs={12}>
        <Grid container spacing={gridSpacing}>
          <Grid item xs={12} md={4}>
            <DonutChart productions={productions} delivery_information={delivery_information} />
          </Grid>
          <Grid item xs={12} md={4}>
            {total_working_hours_per_day && <WeekMonth weeklyFeed={weekly_feed} totalWorkingHoursPerDay={total_working_hours_per_day} />}
          </Grid>
          <Grid item xs={12} md={4}>
            <CopyTotalIncome
              weeklyFeed={weekly_feed}
              totalProductionHours={total_production_hours}
              totalDownTimes={total_down_times}
              total_time_working={total_time_working}
              total_delivery_last_month_by_hour={total_delivery_last_month_by_hour}
            />
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={12}>
        <Grid container spacing={gridSpacing}>
          <Grid item xs={12}>
            <MuiTabs deliveryInfo={delivery_info} />
          </Grid>
        </Grid>
      </Grid>
      */}
      <Grid item xs={12}>
        <Grid container spacing={gridSpacing}>
          <Grid item xs={12}>
            <AssignCards test="1" img={img1} />
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={12} style={{ paddingTop: '10px' }}>
        <Grid container spacing={gridSpacing}>
          <Grid item xs={12}>
            <AssignCards test="2" img={img2} />
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={12} style={{ paddingTop: '10px' }}>
        <Grid container spacing={gridSpacing}>
          <Grid item xs={12}>
            <AssignCards test="3" img={img3} />
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={12} style={{ paddingTop: '10px' }}>
        <Grid container spacing={gridSpacing}>
          <Grid item xs={12}>
            <AssignCards test="4" img={img3} />
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default Dashboard;
