import React from 'react';
import { Grid } from '@mui/material';
import AccessTimeIcon from '@mui/icons-material/AccessTime';
import ApexChart from 'react-apexcharts';

const Card = ({ icon, header, title, grid }) => (
  <Grid item xs={12} md={grid}>
    <div className="card" style={{ display: 'flex', justifyContent: 'space-between' }}>
      <div>
        <h5 className="card__h5" style={{ marginBottom: '20px', fontSize: '13px' }}>
          {header}
        </h5>
        <p className="card--p1">
          {title} <span style={{ fontSize: '14px' }}>{header !== 'Delivery ton/hr' ? 'hr' : '/hr'}</span>
        </p>
      </div>
      <div className="card__title">
        <span>{icon}</span>
      </div>
    </div>
  </Grid>
);

const WeeklyFeedProduction = ({ isAllEmpty, sumQunatity, series, options }) => (
  <Grid item xs={12} md={12}>
    <div className={`card ${isAllEmpty ? 'productionCard12' : ''}`}>
      {isAllEmpty ? (
        <Grid item xs={12} md={12}>
          <div className="">
            <div className="card__date">
              <h4>Produced Ton/Hr</h4>
              <span>Last Week</span>
            </div>
            <div className="noDataContainer">
              <p style={{ marginTop: '70px' }}>No Productions Data</p>
            </div>
          </div>
        </Grid>
      ) : (
        <Grid container spacing={1}>
          <Grid item xs={12} md={5}>
            <h4 className="card__h2">Produced Ton/Hr</h4>
            {/* <p className="card--p1">{Math.floor(sumQunatity)}</p> */}
            <p className="card--p1">{sumQunatity.toFixed(3)}</p>
          </Grid>
          <Grid item xs={12} md={7}>
            <ApexChart options={options} series={series} type="line" height={228} />
          </Grid>
        </Grid>
      )}
    </div>
  </Grid>
);

// const calculateAverageHour = (weeklyFeed) => weeklyFeed?.map((day) => Math.floor(day.avg_hourly_quantity)).slice(-7);
const calculateAverageHour = (weeklyFeed) => weeklyFeed?.map((day) => day.avg_hourly_quantity.toFixed(3)).slice(-7);

const CopyTotalIncome = ({ weeklyFeed, totalProductionHours, totalDownTimes, total_time_working, total_delivery_last_month_by_hour }) => {
  const sumQunatity = weeklyFeed?.slice(-7).reduce((acc, cur) => acc + cur.sum_quantity, 0);
  const avarageHour = calculateAverageHour(weeklyFeed);
  const sumTotal = total_time_working;
  const isAllEmpty = avarageHour?.map((el) => Math.floor(el)).every((el) => el === 0);

  const series = [{ name: 'Ton/Hour', data: avarageHour }];
  const options = {
    chart: {
      type: 'line',
      toolbar: { show: false },
      dropShadow: {
        enabled: true,
        top: 10,
        left: 5,
        blur: 3,
        color: '#FF9F43', // Example warm color
        opacity: 0.15
      },
      sparkline: { enabled: true }
    },
    grid: {
      show: false,
      padding: { right: 8 }
    },
    colors: ['#FF9F43'], // Example warm color
    dataLabels: { enabled: false },
    stroke: {
      width: 5,
      curve: 'smooth'
    },
    xaxis: {
      show: false,
      lines: { show: false },
      labels: { show: false },
      axisBorder: { show: false }
    },
    yaxis: { show: false },
    tooltip: {
      enabled: true,
      y: {
        formatter: (value) => {
          // Check if the value is zero
          if (value === 0) {
            return '0'; // Display '0' for zero values
          }
          return value.toString(); // Convert non-zero values to string
        }
      }
    }
  };

  return (
    <Grid container spacing={2}>
      <Card grid="6" icon={<AccessTimeIcon />} header="Working hrs" title={totalProductionHours} />
      <Card grid="6" icon={<AccessTimeIcon />} header="Total Run time/hrs" title={sumTotal} />
      <Card grid="6" icon={<AccessTimeIcon />} header="Downtime" title={totalDownTimes} />
      <Card grid="6" icon={<AccessTimeIcon />} header="Delivery ton/hr" title={total_delivery_last_month_by_hour.toFixed(3)} />
      <WeeklyFeedProduction isAllEmpty={isAllEmpty} sumQunatity={sumQunatity} series={series} options={options} />
    </Grid>
  );
};

export default CopyTotalIncome;
