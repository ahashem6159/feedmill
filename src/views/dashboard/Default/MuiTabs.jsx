// import { Box, Tab, Fade } from '@mui/material';
// import { TabContext, TabList, TabPanel } from '@mui/lab';
// import { useState } from 'react';
// import Orders from './orders/Orders';
// import CustomLoading from 'common/CustomLoading';

// export const MuiTabs = ({ deliveryInfo }) => {
//   const [value, setValue] = useState('1');

//   if (!deliveryInfo) {
//     return <CustomLoading />;
//   }

//   const handleChange = (event, newValue) => {
//     setValue(newValue);
//   };

//   const tabs = [
//     { label: 'Breeders', value: '1' },
//     { label: 'Broilers', value: '2' }
//   ];

//   const filterBreeders = deliveryInfo?.filter((el) => el.fk_to_breeders_farm__name !== null);
//   const filterBroilers = deliveryInfo?.filter((el) => el.fk_to_breeders_farm__name === null);

//   return (
//     <div className="tabsContainer">
//       <Box>
//         <TabContext value={value}>
//           <Box sx={{ borderBottom: 0 }}>
//             <TabList onChange={handleChange} aria-label="Tabs example" indicatorColor="">
//               {tabs.map((tab) => (
//                 <Tab
//                   key={tab.value}
//                   iconPosition={tab.icon ? 'start' : undefined}
//                   label={tab.label}
//                   value={tab.value}
//                   sx={{
//                     backgroundColor: value === tab.value ? '#248ecc' : 'transparent',
//                     color: value === tab.value ? '#fff !important' : '#555',
//                     borderRadius: value === tab.value ? '5px' : '0',
//                     fontWeight: '300',
//                     fontSize: '15px',
//                     padding: '10px 17px',
//                     minHeight: 'auto',
//                     letterSpacing: '.2px',
//                     boxShadow: value === tab.value ? '0px 2px 4px rgba(0, 0, 0, 0.25)' : 'none',
//                     transition: 'all 0.3s ease' // Transition for smooth effect
//                   }}
//                 />
//               ))}
//             </TabList>
//           </Box>
//           {tabs.map((tab) => (
//             <Fade in={value === tab.value} timeout={500} key={tab.value}>
//               <TabPanel sx={{ padding: '12px 22px 0px' }} value={tab.value}>
//                 {deliveryInfo?.length <= 0 ? (
//                   <div className="fancy">
//                     <p className="fancy-text">Sorry, No Data</p>
//                   </div>
//                 ) : (
//                   <Orders value={value} filterBreeders={filterBreeders} filterBroilers={filterBroilers} />
//                 )}
//               </TabPanel>
//             </Fade>
//           ))}
//         </TabContext>
//       </Box>
//     </div>
//   );
// };

// import React, { useState, useEffect } from 'react';
// import { Box, Tab, Fade } from '@mui/material';
// import { TabContext, TabList, TabPanel } from '@mui/lab';
// import Orders from './orders/Orders';
// import CustomLoading from 'common/CustomLoading';
// import { Pagination } from 'antd';

// export const MuiTabs = ({ deliveryInfo }) => {
//   const [value, setValue] = useState('1');
//   const [page, setPage] = useState(1);
//   const [filteredData, setFilteredData] = useState([]);
//   const itemsPerPage = 5;

//   useEffect(() => {
//     // Function to filter data based on tab selection
//     const filterData = () => {
//       const isBreeder = value === '1';
//       const filtered = deliveryInfo.filter((el) =>
//         isBreeder ? el.fk_to_breeders_farm__name !== null : el.fk_to_breeders_farm__name === null
//       );
//       return filtered.slice((page - 1) * itemsPerPage, page * itemsPerPage);
//     };

//     if (deliveryInfo) {
//       setFilteredData(filterData());
//     }
//   }, [deliveryInfo, value, page]); // Dependency array includes value and page to re-filter on tab change or page change

//   // Handle tab change
//   const handleChange = (event, newValue) => {
//     setValue(newValue);
//     setPage(1); // Reset to page 1 when tab changes
//   };

//   // Handle page change
//   const handlePageChange = (newPage) => {
//     setPage(newPage);
//   };

//   // Tabs configuration
//   const tabs = [
//     { label: 'Breeders', value: '1' },
//     { label: 'Broilers', value: '2' }
//   ];

//   // Count total items for breeders and broilers
//   const totalBreeders = deliveryInfo.filter((el) => el.fk_to_breeders_farm__name !== null).length;
//   const totalBroilers = deliveryInfo.filter((el) => el.fk_to_breeders_farm__name === null).length;

//   // Loading state
//   if (!deliveryInfo) {
//     return <CustomLoading />;
//   }

//   return (
//     <div className="tabsContainer">
//       <Box>
//         <TabContext value={value}>
//           <Box sx={{ borderBottom: 0 }}>
//             <TabList onChange={handleChange} aria-label="Tabs example">
//               {tabs.map((tab) => (
//                 <Tab key={tab.value} label={tab.label} value={tab.value} /* Styling props here */ />
//               ))}
//             </TabList>
//           </Box>
//           {tabs.map((tab) => (
//             <Fade in={value === tab.value} timeout={500} key={tab.value}>
//               <TabPanel value={tab.value} /* Styling props here */>
//                 {filteredData.length <= 0 ? (
//                   <div className="fancy">
//                     <p className="fancy-text">Sorry, No Data</p>
//                   </div>
//                 ) : (
//                   <Orders value={value} filteredData={filteredData} />
//                 )}
//               </TabPanel>
//             </Fade>
//           ))}
//         </TabContext>
//       </Box>
//       <div className="paginationContainer">
//         <Pagination
//           total={value === '1' ? totalBreeders : totalBroilers}
//           pageSize={itemsPerPage}
//           current={page}
//           onChange={handlePageChange}
//           showSizeChanger={false}
//         />
//       </div>
//     </div>
//   );
// };

// export default MuiTabs;

import React, { useState, useEffect } from 'react';
import { Box, Tab, Fade } from '@mui/material';
import { TabContext, TabList, TabPanel } from '@mui/lab';
import Orders from './orders/Orders';
import CustomLoading from 'common/CustomLoading';
import { Pagination } from 'antd';

export const MuiTabs = ({ deliveryInfo }) => {
  const [value, setValue] = useState('1');
  const [page, setPage] = useState(1);
  const [filteredData, setFilteredData] = useState([]);
  const itemsPerPage = 5;

  useEffect(() => {
    const isBreeder = value === '1';
    const filtered = deliveryInfo?.filter((el) =>
      isBreeder ? el.fk_to_breeders_farm__name !== null : el.fk_to_breeders_farm__name === null
    );
    setFilteredData(filtered.slice((page - 1) * itemsPerPage, page * itemsPerPage));
  }, [deliveryInfo, value, page]);

  const handleChange = (event, newValue) => {
    setValue(newValue);
    setPage(1);
  };

  const handlePageChange = (newPage) => setPage(newPage);

  const tabs = [
    { label: 'Breeders', value: '1' },
    { label: 'Broilers', value: '2' }
  ];

  if (!deliveryInfo) return <CustomLoading />;

  const getTotalItems = (isBreeder) =>
    deliveryInfo.filter((el) => (isBreeder ? el.fk_to_breeders_farm__name !== null : el.fk_to_breeders_farm__name === null)).length;

  return (
    <div className="tabsContainer">
      <Box>
        <TabContext value={value}>
          <Box sx={{ borderBottom: 0 }}>
            <TabList onChange={handleChange} aria-label="Tabs example" indicatorColor="">
              {tabs.map((tab) => (
                <Tab
                  key={tab.value}
                  label={tab.label}
                  value={tab.value}
                  sx={{
                    backgroundColor: value === tab.value ? '#248ecc' : 'transparent',
                    color: value === tab.value ? '#fff !important' : '#555',
                    borderRadius: value === tab.value ? '5px' : '0',
                    fontWeight: '300',
                    fontSize: '15px',
                    padding: '10px 17px',
                    minHeight: 'auto',
                    letterSpacing: '.2px',
                    boxShadow: value === tab.value ? '0px 2px 4px rgba(0, 0, 0, 0.25)' : 'none',
                    transition: 'all 0.3s ease'
                  }}
                />
              ))}
            </TabList>
          </Box>
          {tabs.map((tab) => (
            <Fade in={value === tab.value} timeout={500} key={tab.value}>
              <TabPanel value={tab.value}>
                {filteredData.length ? (
                  <Orders filteredData={filteredData} />
                ) : (
                  <div className="fancy">
                    <p className="fancy-text">Sorry, No Data</p>
                  </div>
                )}
              </TabPanel>
            </Fade>
          ))}
        </TabContext>
      </Box>
      <div className="paginationContainer">
        <Pagination
          total={value === '1' ? getTotalItems(true) : getTotalItems(false)}
          pageSize={itemsPerPage}
          current={page}
          onChange={handlePageChange}
          showSizeChanger={false}
        />
      </div>
    </div>
  );
};

export default MuiTabs;
