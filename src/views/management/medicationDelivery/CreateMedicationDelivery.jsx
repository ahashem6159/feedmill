import React, { useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import Button from '@mui/material/Button';
import IosShareIcon from '@mui/icons-material/IosShare';
import ArrowCircleLeftIcon from '@mui/icons-material/ArrowCircleLeft';

// eslint-disable-next-line no-restricted-imports
import { DemoContainer } from '@mui/x-date-pickers/internals/demo';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';

// Import Material Ui
import { Grid } from '@mui/material';
import CustomInput from '../../../common/CustomInput';
import CustomSelect from '../../../common/CustomSelect';
import CustomButton from 'common/CustomButton';
import deleteHandler from 'helper/DeleteAlert';

// Functions & Variables

const names = [
  'Oliver Hansen',
  'Van Henry',
  'April Tucker',
  'Ralph Hubbard',
  'Omar Alexander',
  'Carlos Abbott',
  'Miriam Wagner',
  'Bradley Wilkerson',
  'Virginia Andrews',
  'Kelly Snyder'
];

// Component
const CreateMedicationDelivery = () => {
  const [personName, setPersonName] = useState([]);
  const { id } = useParams();

  // const rowArr = [{ id: 1, ingredient: ['Oliver Hansen'], kgs: '' }];
  // console.log(rowArr);

  const handleChange = (event) => {
    const {
      target: { value }
    } = event;
    setPersonName(
      // On autofill we get a stringified value.
      typeof value === 'string' ? value.split(',') : value
    );
  };

  const formulaName = id ? `Edit Medication Delivery / ${id}` : 'Create Medication Delivery';

  const exportFormulaBtn = (
    <span className="createformula__link">
      <Button className="formula__buttons--button" variant="outlined" startIcon={<IosShareIcon />}>
        Export Formula
      </Button>
    </span>
  );

  return (
    <>
      <div className="createformula">
        <h1 className="createformula__title">{formulaName}</h1>
        <div className="createformula__links">
          <Link to={`/delivery/MedicationDelivery`} className="formula__Back">
            <Button className="formula__buttons--button" variant="outlined" startIcon={<ArrowCircleLeftIcon />}>
              Back
            </Button>
          </Link>
          {id && exportFormulaBtn}
        </div>
      </div>
      <div className="formula__information">
        <h3 className="createformula--heading">Medication Delivery Information</h3>
        <Grid container spacing={1}>
          <Grid item xs={4}>
            <div className="formula__information__parent">
              <CustomSelect title="Medicine Name" label="Parent Formula" value={personName} onChange={handleChange} names={names} />
              <CustomInput title="Quantity" type="number" />
            </div>
          </Grid>
          <Grid item xs={4}>
            <div className="formula__information__details">
              <div className="formula__information__parent__type">
                <p className="date">Delivery Date</p>
                <LocalizationProvider dateAdapter={AdapterDayjs}>
                  <DemoContainer components={['DatePicker']}>
                    <DatePicker label="Date" slotProps={{ textField: { size: 'small' } }} sx={{ width: '100%' }} />
                  </DemoContainer>
                </LocalizationProvider>
              </div>
              <div className="formula__information__parent__type">
                <p className="date">Expiry Date</p>
                <LocalizationProvider dateAdapter={AdapterDayjs}>
                  <DemoContainer components={['DatePicker']}>
                    <DatePicker label="Date" slotProps={{ textField: { size: 'small' } }} sx={{ width: '100%' }} />
                  </DemoContainer>
                </LocalizationProvider>
              </div>
            </div>
          </Grid>
          <Grid item xs={4}>
            <div className="formula__information__details">
              <CustomInput title="Medication for Age" type="number" />
              <CustomInput title="Serial Number" type="text" />
            </div>
          </Grid>
          <Grid item xs={4}>
            <div className="formula__information__details">
              <CustomInput title="Withdrawal Period (Number of days)" type="number" />
              <CustomInput title="Cost" type="number" />
            </div>
          </Grid>
          <Grid item xs={4}>
            <div className="formula__information__details">
              <CustomInput title="Cost per unit" type="number" />
              <CustomInput title="Batch Number" type="text" />
            </div>
          </Grid>
        </Grid>
      </div>
      <div className="customButtons">
        <CustomButton title="Save" />
        {id && <CustomButton color="error" title="Delete" onClick={deleteHandler} />}
      </div>
    </>
  );
};

export default CreateMedicationDelivery;
