import React from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { DataGrid } from '@mui/x-data-grid';
import Button from '@mui/material/Button';
import DeleteIcon from '@mui/icons-material/Delete';
import { useState } from 'react';

const columns = [
  { field: 'id', headerName: 'ID', width: 70 },
  { field: 'madicineName', headerName: 'Madicine Name', width: 130 },
  { field: 'deliveryDate', headerName: 'Delivery Date', width: 130 },
  { field: 'quantity', headerName: 'Quantity', width: 130 },
  { field: 'medication', headerName: 'Medication For Age', width: 130 },
  { field: 'serialNumber', headerName: 'Serial Number', width: 130 },
  { field: 'cost', headerName: 'Cost', width: 130 },
  { field: 'batchNumber', headerName: 'Batch Number', width: 130 }
];

const rows = [
  {
    id: 1,
    madicineName: 'Bags',
    deliveryDate: 'June 24, 2023',
    expiryDate: 'Jan. 30, 2024',
    quantity: '2,700.0',
    medication: '-',
    serialNumber: '-',
    cost: 12,
    batchNumber: '-'
  }
];

const MedicationDelivery = () => {
  const navigate = useNavigate();
  const [selectedRowIds, setSelectedRowIds] = useState([]);

  let isSelectedAll = rows.length === selectedRowIds.length;

  const handleStateChange = (state) => {
    setSelectedRowIds(state.rowSelection);
  };

  const handleRowClick = (params) => {
    const id = params.row.id;
    navigate(`/delivery/MedicationDelivery/edit/${id}`);
  };

  return (
    <div>
      <div className="formula">
        <h1 className="formula__h1">Medication Delivery</h1>
        <span className="formula__link">
          <Link to={`/delivery/MedicationDelivery/create`}>
            <Button variant="contained">Create Medication Delivery</Button>
          </Link>
        </span>
      </div>
      {selectedRowIds.length > 0 && (
        <div className="formula__buttons">
          <Button className="formula__buttons--button" variant="outlined" startIcon={<DeleteIcon />}>
            {isSelectedAll ? 'Delete All Items' : 'Delete Selected Items'}
          </Button>
        </div>
      )}
      <div style={{ height: 400, width: '100%' }}>
        <DataGrid
          style={{ background: '#fff' }}
          rows={rows}
          columns={columns}
          onRowClick={handleRowClick}
          onStateChange={handleStateChange}
          initialState={{
            pagination: {
              paginationModel: { page: 0, pageSize: 5 }
            }
          }}
          pageSizeOptions={[5, 10]}
          checkboxSelection
        />
      </div>
    </div>
  );
};

export default MedicationDelivery;
