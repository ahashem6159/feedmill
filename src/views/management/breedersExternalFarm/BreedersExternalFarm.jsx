import React from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { DataGrid } from '@mui/x-data-grid';
import Button from '@mui/material/Button';
import DeleteIcon from '@mui/icons-material/Delete';
import { useState } from 'react';

const columns = [
  { field: 'id', headerName: 'ID', width: 70 },
  { field: 'FormulaName', headerName: 'Formula Name', width: 130 },
  { field: 'Code', headerName: 'Code', width: 130 },
  {
    field: 'FormulaType',
    headerName: 'Formula Type',
    width: 130
  },
  {
    field: 'FeedType',
    headerName: 'Feed Type',
    width: 130
  },
  {
    field: 'RoundedWeight',
    headerName: 'Rounded Weight',
    width: 130
  },
  {
    field: 'DateTime',
    headerName: 'Creation Date Time',
    width: 130
  },
  {
    field: 'Status',
    headerName: 'Status',
    width: 130
  }
];

const rows = [
  {
    id: 1,
    FormulaName: 'Test',
    Code: '777',
    FormulaType: 'Internal',
    FeedType: 'Grower',
    RoundedWeight: 1,
    DateTime: new Date(),
    Status: 'Active'
  },
  {
    id: 2,
    FormulaName: '0-Int-Grower L23',
    Code: 'IBF/L23	',
    FormulaType: 'Internal',
    FeedType: 'Finisher',
    RoundedWeight: 1,
    DateTime: new Date(),
    Status: 'Active'
  },
  {
    id: 3,
    FormulaName: '3-Ext-Finisher E6',
    Code: 'IBG/L23',
    FormulaType: 'Internal',
    FeedType: 'Grower',
    RoundedWeight: 1,
    DateTime: new Date(),
    Status: 'Active'
  },
  {
    id: 4,
    FormulaName: '3-Ext-Starter E6',
    Code: 'IBS/L23',
    FormulaType: 'Internal',
    FeedType: 'Starter',
    RoundedWeight: 1,
    DateTime: new Date(),
    Status: 'Active'
  },
  {
    id: 5,
    FormulaName: 'Broiler PreStarter',
    Code: 'IBBS/L22',
    FormulaType: 'Internal',
    FeedType: 'Broiler Starter',
    RoundedWeight: 1,
    DateTime: new Date(),
    Status: 'Active'
  },
  {
    id: 6,
    FormulaName: 'Internal Finisher',
    Code: 'IBF/L21',
    FormulaType: 'Internal',
    FeedType: 'Finisher',
    RoundedWeight: 1,
    DateTime: new Date(),
    Status: 'Active'
  }
];

const BreedersExternalFarm = () => {
  const navigate = useNavigate();
  const [selectedRowIds, setSelectedRowIds] = useState([]);

  let isSelectedAll = rows.length === selectedRowIds.length;

  const handleStateChange = (state) => {
    setSelectedRowIds(state.rowSelection);
  };

  const handleRowClick = (params) => {
    const id = params.row.id;
    navigate(`/breeders/BreedersExternalFarm/edit/${id}`);
  };

  return (
    <div>
      <div className="formula">
        <h1 className="formula__h1">Breeders External Farm</h1>
        <span className="formula__link">
          <Link to={`/breeders/BreedersExternalFarm/create`}>
            <Button variant="contained">Create Breeders External Farm</Button>
          </Link>
        </span>
      </div>
      {selectedRowIds.length > 0 && (
        <div className="formula__buttons">
          <Button className="formula__buttons--button" variant="outlined" startIcon={<DeleteIcon />}>
            {isSelectedAll ? 'Delete All Items' : 'Delete Selected Items'}
          </Button>
        </div>
      )}
      <div style={{ height: 400, width: '100%' }}>
        <DataGrid
          style={{ background: '#fff' }}
          rows={rows}
          columns={columns}
          onRowClick={handleRowClick}
          onStateChange={handleStateChange}
          initialState={{
            pagination: {
              paginationModel: { page: 0, pageSize: 5 }
            }
          }}
          pageSizeOptions={[5, 10]}
          checkboxSelection
        />
      </div>
    </div>
  );
};

export default BreedersExternalFarm;
