import React from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { DataGrid } from '@mui/x-data-grid';
import Button from '@mui/material/Button';
import DeleteIcon from '@mui/icons-material/Delete';
import { useState } from 'react';

const columns = [
  { field: 'id', headerName: 'ID', width: 70 },
  {
    field: 'DateTime',
    headerName: 'Date',
    width: 130
  },
  { field: 'Formula', headerName: 'Formula', width: 130 },
  {
    field: 'FeedType',
    headerName: 'Feed Type',
    width: 130
  },
  { field: 'QuantityLeft', headerName: 'Quantity Left', width: 130 },
  {
    field: 'TotalWorkingHours',
    headerName: 'Total Working Hours',
    width: 130
  },
  {
    field: 'TotalDownTime',
    headerName: 'Total Down Time',
    width: 130
  },
  {
    field: 'DowntimeReasons',
    headerName: 'Down time Reasons',
    width: 130
  }
];

const rows = [
  {
    id: 1,
    Formula: 'Test',
    QuantityLeft: '208.015',
    FeedType: 'Grower',
    TotalWorkingHours: 1,
    DateTime: new Date(),
    TotalDownTime: '12',
    DowntimeReasons: '-'
  }
];

const Production = () => {
  const navigate = useNavigate();
  const [selectedRowIds, setSelectedRowIds] = useState([]);

  let isSelectedAll = rows.length === selectedRowIds.length;

  const handleStateChange = (state) => {
    setSelectedRowIds(state.rowSelection);
  };

  const handleRowClick = (params) => {
    const id = params.row.id;
    navigate(`/administration/Production/edit/${id}`);
  };

  return (
    <div>
      <div className="formula">
        <h1 className="formula__h1">Production</h1>
        <span className="formula__link">
          <Link to={`/administration/Production/create`}>
            <Button variant="contained">Create Production</Button>
          </Link>
        </span>
      </div>
      {selectedRowIds.length > 0 && (
        <div className="formula__buttons">
          <Button className="formula__buttons--button" variant="outlined" startIcon={<DeleteIcon />}>
            {isSelectedAll ? 'Delete All Items' : 'Delete Selected Items'}
          </Button>
        </div>
      )}
      <div style={{ height: 400, width: '100%' }}>
        <DataGrid
          style={{ background: '#fff' }}
          rows={rows}
          columns={columns}
          onRowClick={handleRowClick}
          onStateChange={handleStateChange}
          initialState={{
            pagination: {
              paginationModel: { page: 0, pageSize: 5 }
            }
          }}
          pageSizeOptions={[5, 10]}
          checkboxSelection
        />
      </div>
    </div>
  );
};

export default Production;

/* const columns = [
  { field: 'id', headerName: 'ID', width: 70 },
  {
    field: 'DateTime',
    headerName: 'Date',
    width: 120
  },
  {
    field: 'quantityTons',
    headerName: 'Quantity In Tons',
    width: 150
  },
  {
    field: 'quantityLeft',
    headerName: 'Quantity Left',
    width: 150
  },
  {
    field: 'totalWorkingHours',
    headerName: 'Total Working Hours',
    width: 150
  },
  {
    field: 'totalDownTime',
    headerName: 'total Down Time',
    width: 150
  }
];

const rows = [
  {
    id: 1,
    DateTime: new Date(),
    quantityTons: 566.03,
    quantityLeft: 566.02,
    totalWorkingHours: 96.03,
    totalDownTime: 1
  }
]; */
