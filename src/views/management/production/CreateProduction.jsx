import React, { useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import Button from '@mui/material/Button';
// import IosShareIcon from '@mui/icons-material/IosShare';
import ArrowCircleLeftIcon from '@mui/icons-material/ArrowCircleLeft';

import { Grid } from '@mui/material';
import CustomInput from 'common/CustomInput';
import deleteHandler from 'helper/DeleteAlert';
import CustomButtons from 'common/CustomButtons';
import CustomDataPicker from 'common/CustomDataPicker';
import CustomDateTimePicker from 'common/CustomDateTimePicker';
import CustomSelectSearch from 'common/CustomSelectSearch';

const supplierData = ['Golden Chicken', 'Taya Feed Mill', 'Arasco Feed Mill'];

// Component
const CreateProduction = () => {
  const { id } = useParams();
  const [selectedValue, setSelectedValue] = useState(null);

  const handleAutocompleteChange = (event, newValue) => {
    console.log(newValue);
    setSelectedValue('');
  };

  const formulaName = id ? `Edit Production / ${id}` : 'Create Production';

  return (
    <>
      <div className="createformula">
        <h1 className="createformula__title">{formulaName}</h1>
        <div className="createformula__links">
          <Link to={`/administration/Production`} className="formula__Back">
            <Button className="formula__buttons--button" variant="outlined" startIcon={<ArrowCircleLeftIcon />}>
              Back
            </Button>
          </Link>
        </div>
      </div>
      <div className="formula__information">
        <h3 className="createformula--heading">Production</h3>
        <Grid container spacing={1}>
          <Grid item xs={6}>
            <div className="formula__information__details">
              <div style={{ display: 'flex' }}>
                <CustomSelectSearch title="Formula" data={supplierData} value={selectedValue} onChange={handleAutocompleteChange} />
                <CustomInput title="Quantity in Tons" type="number" />
              </div>
              <div className="formula__information__parent__type">
                <p className="date">Date</p>
                <CustomDataPicker />
              </div>
            </div>
          </Grid>
          <Grid item xs={6}>
            <div className="formula__information__details">
              <div className="formula__information__details__date">
                <div className="formula__information__parent__type">
                  <p className="startDate">Start Date</p>
                  <CustomDateTimePicker />
                </div>
                <div className="formula__information__parent__type">
                  <p className="endDate">End Date</p>
                  <CustomDateTimePicker />
                </div>
              </div>
            </div>
          </Grid>
        </Grid>
      </div>
      <CustomButtons id={id} deleteHandler={deleteHandler} />
    </>
  );
};

export default CreateProduction;
