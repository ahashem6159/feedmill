import React from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { DataGrid } from '@mui/x-data-grid';
import Button from '@mui/material/Button';
import DeleteIcon from '@mui/icons-material/Delete';
import { useState } from 'react';

const columns = [
  { field: 'id', headerName: 'ID', width: 70 },
  { field: 'farm', headerName: 'To Farm', width: 150 },
  { field: 'month', headerName: 'Month', width: 150 },
  { field: 'cycle', headerName: 'CYyle', width: 150 },
  { field: 'feedType', headerName: 'Feed Type', width: 150 },
  { field: 'quantityReq', headerName: 'Quantity Requested In Tons', width: 150 },
  { field: 'quantityRecieved', headerName: 'Quantity Received In Tons', width: 150 }
];

const rows = [
  {
    id: 1,
    farm: 'Adnan',
    month: 'November',
    cycle: 247,
    feedType: 'Grower',
    quantityReq: 225.0,
    quantityRecieved: 175.97
  }
];

const BreedersFeedRequests = () => {
  const navigate = useNavigate();
  const [selectedRowIds, setSelectedRowIds] = useState([]);

  let isSelectedAll = rows.length === selectedRowIds.length;

  const handleStateChange = (state) => {
    setSelectedRowIds(state.rowSelection);
  };

  const handleRowClick = (params) => {
    const id = params.row.id;
    navigate(`/breeders/BreedersFeedRequests/edit/${id}`);
  };

  const handleFilterModelChange = (newFilterModel) => {
    console.log('Filter model changed:', newFilterModel);
  };

  return (
    <div>
      <div className="formula">
        <h1 className="formula__h1">Breeders Feed Requests</h1>
        <span className="formula__link">
          <Link to={`/breeders/BreedersFeedRequests/create`}>
            <Button variant="contained">Breeders Feed Requests</Button>
          </Link>
        </span>
      </div>
      {selectedRowIds.length > 0 && (
        <div className="formula__buttons">
          <Button className="formula__buttons--button" variant="outlined" startIcon={<DeleteIcon />}>
            {isSelectedAll ? 'Delete All Items' : 'Delete Selected Items'}
          </Button>
        </div>
      )}
      <div style={{ height: 400, width: '100%' }}>
        <DataGrid
          style={{ background: '#fff' }}
          rows={rows}
          columns={columns}
          onRowClick={handleRowClick}
          onStateChange={handleStateChange}
          initialState={{
            pagination: {
              paginationModel: { page: 0, pageSize: 5 }
            }
          }}
          pageSizeOptions={[5, 10]}
          checkboxSelection
          onFilterModelChange={handleFilterModelChange}
        />
      </div>
    </div>
  );
};

export default BreedersFeedRequests;
