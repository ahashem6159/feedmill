import React from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { DataGrid } from '@mui/x-data-grid';
import Button from '@mui/material/Button';
import DeleteIcon from '@mui/icons-material/Delete';
import { useState } from 'react';

const columns = [
  { field: 'id', headerName: 'ID', width: 70 },
  { field: 'truckCode', headerName: 'Truck Code', width: 130 }
];

const rows = [
  { id: 1, truckCode: '5794 NBB' },
  { id: 2, truckCode: '4506 VRA' },
  { id: 3, truckCode: '16101' },
  { id: 4, truckCode: '2090 TDB' },
  { id: 5, truckCode: '2825 NDB' }
];

const Truck = () => {
  const navigate = useNavigate();
  const [selectedRowIds, setSelectedRowIds] = useState([]);

  let isSelectedAll = rows.length === selectedRowIds.length;

  const handleStateChange = (state) => {
    setSelectedRowIds(state.rowSelection);
  };

  const handleRowClick = (params) => {
    const id = params.row.id;
    navigate(`/reports/Truck/edit/${id}`);
  };

  return (
    <div>
      <div className="formula">
        <h1 className="formula__h1">Truck</h1>
        <span className="formula__link">
          <Link to={`/reports/Truck/create`}>
            <Button variant="contained">Create Truck</Button>
          </Link>
        </span>
      </div>
      {selectedRowIds.length > 0 && (
        <div className="formula__buttons">
          <Button className="formula__buttons--button" variant="outlined" startIcon={<DeleteIcon />}>
            {isSelectedAll ? 'Delete All Items' : 'Delete Selected Items'}
          </Button>
        </div>
      )}
      <div style={{ height: 400, width: '100%' }}>
        <DataGrid
          style={{ background: '#fff' }}
          rows={rows}
          columns={columns}
          onRowClick={handleRowClick}
          onStateChange={handleStateChange}
          initialState={{
            pagination: {
              paginationModel: { page: 0, pageSize: 5 }
            }
          }}
          pageSizeOptions={[5, 10]}
          checkboxSelection
        />
      </div>
    </div>
  );
};

export default Truck;
