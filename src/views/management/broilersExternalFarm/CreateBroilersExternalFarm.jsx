import React, { useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import Button from '@mui/material/Button';
import IosShareIcon from '@mui/icons-material/IosShare';
import ArrowCircleLeftIcon from '@mui/icons-material/ArrowCircleLeft';

import { Grid } from '@mui/material';
import CustomInput from '../../../common/CustomInput';
import deleteHandler from 'helper/DeleteAlert';
import NutrientsInformation from '../formula/NutrientsInformation';
import RawMaterial from '../formula/RawMaterial';
import CustomSelectSearch from 'common/CustomSelectSearch';
import CustomButtons from 'common/CustomButtons';

// Functions & Variables
const top100Films = ['The Shawshank Redemption', 'The Dark Knight', 'The Godfather'];

// Component
const CreateBroilersExternalFarm = () => {
  const { id } = useParams();

  const [selectedValue, setSelectedValue] = useState(null);

  const handleAutocompleteChange = (event, newValue) => {
    setSelectedValue(newValue);
  };

  const formulaName = id ? `Edit Boilers External Farm / ${id}` : 'Create Boilers External Farm';

  const exportFormulaBtn = (
    <span className="createformula__link">
      <Button className="formula__buttons--button" variant="outlined" startIcon={<IosShareIcon />}>
        Export Formula
      </Button>
    </span>
  );

  return (
    <>
      <div className="createformula">
        <h1 className="createformula__title">{formulaName}</h1>
        <div className="createformula__links">
          <Link to={`/broilers/BroilersExternalFarm`} className="formula__Back">
            <Button className="formula__buttons--button" variant="outlined" startIcon={<ArrowCircleLeftIcon />}>
              Back
            </Button>
          </Link>
          {id && exportFormulaBtn}
        </div>
      </div>
      <div className="formula__information">
        <h3 className="createformula--heading">Formula Information</h3>
        <Grid container spacing={1}>
          <Grid item xs={4}>
            <div className="formula__information__parent">
              <CustomSelectSearch title="Parent Formula" data={top100Films} value={selectedValue} onChange={handleAutocompleteChange} />
              <CustomSelectSearch title="Formula Type" data={top100Films} value={selectedValue} onChange={handleAutocompleteChange} />
            </div>
          </Grid>
          <Grid item xs={4}>
            <div className="formula__information__details">
              <CustomInput title="Formula Name" type="text" />
              <CustomInput title="Wheight in tons" type="text" />
            </div>
          </Grid>
          <Grid item xs={4}>
            <div className="formula__information__details">
              <CustomSelectSearch title="Feed Type" data={top100Films} value={selectedValue} onChange={handleAutocompleteChange} />
              <CustomInput title="Code" type="text" />
            </div>
          </Grid>
        </Grid>
      </div>
      <NutrientsInformation />
      <RawMaterial value={selectedValue} onChange={handleAutocompleteChange} names={top100Films} />
      <CustomButtons id={id} deleteHandler={deleteHandler} />
    </>
  );
};

export default CreateBroilersExternalFarm;
