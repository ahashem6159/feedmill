import React, { useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import Button from '@mui/material/Button';
import ArrowCircleLeftIcon from '@mui/icons-material/ArrowCircleLeft';

// Import MUI
import { Grid } from '@mui/material';

// Import Components
import CustomInput from 'common/CustomInput';
import CustomButtons from 'common/CustomButtons';
import CustomSelectSearch from 'common/CustomSelectSearch';

// Helper Functions & Variables
import deleteHandler from 'helper/DeleteAlert';

const feedTypeDate = ['Broilers', 'Breeders'];

// Component
const CreateFeedType = () => {
  const { id } = useParams();
  const [selectedValue, setSelectedValue] = useState(null);

  const handleAutocompleteChange = (event, newValue) => {
    console.log(newValue);
    setSelectedValue('');
  };

  const formulaName = id ? `Edit Feed Type / ${id}` : 'Create Feed Type';

  return (
    <>
      <div className="createformula">
        <h1 className="createformula__title">{formulaName}</h1>
        <div className="createformula__links">
          <Link to={`/reports/FeedType`} className="formula__Back">
            <Button className="formula__buttons--button" variant="outlined" startIcon={<ArrowCircleLeftIcon />}>
              Back
            </Button>
          </Link>
        </div>
      </div>
      <div className="formula__information">
        <h3 className="createformula--heading">Feed</h3>
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <div className="formula__information__details">
              <CustomInput title="Name" type="text" />
              <CustomSelectSearch title="Feed Type" data={feedTypeDate} value={selectedValue} onChange={handleAutocompleteChange} />
              <CustomInput title="Short Code" type="text" />
            </div>
          </Grid>
        </Grid>
      </div>
      <CustomButtons id={id} deleteHandler={deleteHandler} />
    </>
  );
};

export default CreateFeedType;
