import React from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { DataGrid } from '@mui/x-data-grid';
import Button from '@mui/material/Button';
import DeleteIcon from '@mui/icons-material/Delete';
import { useState } from 'react';

const columns = [
  { field: 'id', headerName: 'ID', width: 70 },
  { field: 'name', headerName: 'Name', width: 130 },
  { field: 'type', headerName: 'Type', width: 130 }
];

const rows = [
  { id: 1, name: 'Breeder Male Mash', type: 'Breeders' },
  { id: 2, name: 'Breeder Production 02 Mash', type: 'Breeders' },
  { id: 3, name: 'Breeder Production 01 Mash', type: 'Breeders' },
  { id: 4, name: 'Breeder Grower Mash', type: 'Breeders' },
  { id: 5, name: 'Breeder Starter Mash', type: 'Breeders' },
  { id: 6, name: 'Broiler Starter', type: 'Breeders' },
  { id: 7, name: 'Finisher', type: 'Broilers' }
];

const FeedType = () => {
  const navigate = useNavigate();
  const [selectedRowIds, setSelectedRowIds] = useState([]);

  let isSelectedAll = rows.length === selectedRowIds.length;

  const handleStateChange = (state) => {
    setSelectedRowIds(state.rowSelection);
  };

  const handleRowClick = (params) => {
    const id = params.row.id;
    navigate(`/reports/FeedType/edit/${id}`);
  };

  return (
    <div>
      <div className="formula">
        <h1 className="formula__h1">Feed Type</h1>
        <span className="formula__link">
          <Link to={`/reports/FeedType/create`}>
            <Button variant="contained">Create Feed Type</Button>
          </Link>
        </span>
      </div>
      {selectedRowIds.length > 0 && (
        <div className="formula__buttons">
          <Button className="formula__buttons--button" variant="outlined" startIcon={<DeleteIcon />}>
            {isSelectedAll ? 'Delete All Items' : 'Delete Selected Items'}
          </Button>
        </div>
      )}
      <div style={{ height: 400, width: '100%' }}>
        <DataGrid
          style={{ background: '#fff' }}
          rows={rows}
          columns={columns}
          onRowClick={handleRowClick}
          onStateChange={handleStateChange}
          initialState={{
            pagination: {
              paginationModel: { page: 0, pageSize: 5 }
            }
          }}
          pageSizeOptions={[5, 10]}
          checkboxSelection
        />
      </div>
    </div>
  );
};

export default FeedType;
