import React from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { DataGrid } from '@mui/x-data-grid';
import Button from '@mui/material/Button';
import DeleteIcon from '@mui/icons-material/Delete';
import { useState } from 'react';

const columns = [
  { field: 'id', headerName: 'ID', width: 70 },
  { field: 'farm', headerName: 'To Farm', width: 130 },
  { field: 'name', headerName: 'Name', width: 130 },
  { field: 'itemCode', headerName: 'Item Code', width: 130 },
  { field: 'unit', headerName: 'Unit', width: 130 },
  { field: 'dosesPerTon', headerName: 'Doses Per Ton', width: 130 },
  { field: 'quantityPerPackages', headerName: 'Quantity Per Packages', width: 130 }
];

const rows = [
  {
    id: 1,
    farm: 'Breeders',
    name: 'Denaguard',
    itemCode: '1',
    unit: 'litre',
    dosesPerTon: '5.0',
    quantityPerPackages: 12
  }
];

const MedicationInfo = () => {
  const navigate = useNavigate();
  const [selectedRowIds, setSelectedRowIds] = useState([]);

  let isSelectedAll = rows.length === selectedRowIds.length;

  const handleStateChange = (state) => {
    setSelectedRowIds(state.rowSelection);
  };

  const handleRowClick = (params) => {
    const id = params.row.id;
    navigate(`/administration/MedicationInfo/edit/${id}`);
  };

  const handleFilterModelChange = (newFilterModel) => {
    // Perform actions based on the new filter model
    console.log('Filter model changed:', newFilterModel);
  };

  return (
    <div>
      <div className="formula">
        <h1 className="formula__h1">Medication Info</h1>
        <span className="formula__link">
          <Link to={`/administration/MedicationInfo/create`}>
            <Button variant="contained">Create Medication Info</Button>
          </Link>
        </span>
      </div>
      {selectedRowIds.length > 0 && (
        <div className="formula__buttons">
          <Button className="formula__buttons--button" variant="outlined" startIcon={<DeleteIcon />}>
            {isSelectedAll ? 'Delete All Items' : 'Delete Selected Items'}
          </Button>
        </div>
      )}
      <div style={{ height: 400, width: '100%' }}>
        <DataGrid
          style={{ background: '#fff' }}
          rows={rows}
          columns={columns}
          onRowClick={handleRowClick}
          onStateChange={handleStateChange}
          initialState={{
            pagination: {
              paginationModel: { page: 0, pageSize: 5 }
            }
          }}
          pageSizeOptions={[5, 10]}
          checkboxSelection
          onFilterModelChange={handleFilterModelChange}
        />
      </div>
    </div>
  );
};

export default MedicationInfo;
