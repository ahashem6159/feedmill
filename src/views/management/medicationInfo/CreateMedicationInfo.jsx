import React, { useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import Button from '@mui/material/Button';
import IosShareIcon from '@mui/icons-material/IosShare';
import ArrowCircleLeftIcon from '@mui/icons-material/ArrowCircleLeft';

// Import Material Ui
import { Grid } from '@mui/material';
import CustomInput from '../../../common/CustomInput';
import deleteHandler from 'helper/DeleteAlert';
import CustomButtons from 'common/CustomButtons';
import CustomSelectSearch from 'common/CustomSelectSearch';

// Functions || Variables || Array
const MedicationData = ['Broilers', 'breeders', 'Both'];

// Component
const CreateMedicationInfo = () => {
  const { id } = useParams();

  const [selectedValue, setSelectedValue] = useState(null);

  const handleAutocompleteChange = (event, newValue) => {
    console.log(newValue);
    setSelectedValue('');
  };

  const formulaName = id ? `Edit Medication Info / ${id}` : 'Create Medication Info';

  const exportFormulaBtn = (
    <span className="createformula__link">
      <Button className="formula__buttons--button" variant="outlined" startIcon={<IosShareIcon />}>
        Export Formula
      </Button>
    </span>
  );

  return (
    <>
      <div className="createformula">
        <h1 className="createformula__title">{formulaName}</h1>
        <div className="createformula__links">
          <Link to={`/administration/MedicationInfo`} className="formula__Back">
            <Button className="formula__buttons--button" variant="outlined" startIcon={<ArrowCircleLeftIcon />}>
              Back
            </Button>
          </Link>
          {id && exportFormulaBtn}
        </div>
      </div>
      <div className="formula__information">
        <h3 className="createformula--heading">Medication Info Information</h3>
        <Grid container spacing={1}>
          <Grid item xs={4}>
            <div className="formula__information__parent">
              <CustomSelectSearch title="To Farm" data={MedicationData} value={selectedValue} onChange={handleAutocompleteChange} />
              <CustomInput title="Name" type="text" />
            </div>
          </Grid>
          <Grid item xs={4}>
            <div className="formula__information__details">
              <CustomInput title="Item Code" type="text" />
              <CustomInput title="Unit" type="text" />
            </div>
          </Grid>
          <Grid item xs={4}>
            <div className="formula__information__details">
              <CustomInput title="Doses Per Ton" type="number" />
              <CustomInput title="Quantity Per packages" type="number" />
            </div>
          </Grid>
          <Grid item xs={4}>
            <div className="formula__information__details">
              <CustomInput title="Description" type="text" />
            </div>
          </Grid>
        </Grid>
      </div>
      <CustomButtons id={id} deleteHandler={deleteHandler} />
    </>
  );
};

export default CreateMedicationInfo;
