import React from 'react';
import { Link, useParams } from 'react-router-dom';
import Button from '@mui/material/Button';
import ArrowCircleLeftIcon from '@mui/icons-material/ArrowCircleLeft';

import { Grid } from '@mui/material';
import CustomInput from 'common/CustomInput';
import CustomButton from 'common/CustomButton';

// Functions & Variables
import deleteHandlerAlert from 'helper/DeleteAlert';

// Component
const CreateTransportationFeedDelivery = () => {
  const { id } = useParams();

  const formulaName = id ? `Edit Transportation Feed Delivery / ${id}` : 'Create Transportation Feed Delivery';

  return (
    <>
      <div className="createformula">
        <h1 className="createformula__title">{formulaName}</h1>
        <div className="createformula__links">
          <Link to={`/delivery/TransportationFeedDelivery`} className="formula__Back">
            <Button className="formula__buttons--button" variant="outlined" startIcon={<ArrowCircleLeftIcon />}>
              Back
            </Button>
          </Link>
        </div>
      </div>
      <div className="formula__information">
        <h3 className="createformula--heading">Feed Delivery Request</h3>
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <div className="formula__information__details">
              <CustomInput title="Transportaion Info" type="text" />
              <CustomInput title="Status" type="text" />
            </div>
          </Grid>
        </Grid>
      </div>
      <div className="customButtons">
        <CustomButton title="Save" />
        {id && <CustomButton color="error" title="Delete" onClick={deleteHandlerAlert} />}
      </div>
    </>
  );
};

export default CreateTransportationFeedDelivery;
