import React from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { DataGrid } from '@mui/x-data-grid';
import Button from '@mui/material/Button';
import DeleteIcon from '@mui/icons-material/Delete';
import { useState } from 'react';

const columns = [
  { field: 'id', headerName: 'ID', width: 70 },
  { field: 'transportationInfo', headerName: 'transportation Info', width: 500 },
  { field: 'status', headerName: 'Status', width: 130 }
];

const rows = [
  {
    id: 1,
    transportationInfo:
      '1772 ARA - Nirmal Singh - 2402140509 - Broilers - Golden Chicken - 3-Ext-Starter E6 - IBS/L23 - sara - Starter - Bulk',
    status: 'Approved'
  },
  {
    id: 2,
    transportationInfo:
      '2092 TDB - Tabrez Akhtar - 2450337015 - Broilers - Golden Chicken - 0-Int-Grower L23 - IBF/L23 - Hamada 2 - Finisher - Bulk',
    status: 'Approved'
  },
  {
    id: 3,
    transportationInfo:
      '8675 XSB - Zeeshan Khan - 2488730702 - Broilers - Golden Chicken - 0-Int-Grower L23 - IBF/L23 - Hamada 1 - Finisher - Bulk',
    status: 'Approved'
  },
  {
    id: 4,
    transportationInfo:
      '2894 ESB - Shafique Ahmad - 2414466066 - Broilers - Golden Chicken - 0-Int-Grower L23 - IBF/L23 - leen - Finisher - Bulk',
    status: 'Approved'
  },
  {
    id: 5,
    transportationInfo:
      '5073 JRA - Nazim Hussain - 2450586835 - Broilers - Golden Chicken - 0-Int-Grower L23 - IBF/L23 - Hamada 2 - Finisher - Bulk',
    status: 'Approved'
  },
  {
    id: 6,
    transportationInfo:
      '1772 ARA - Nirmal Singh - 2402140509 - Broilers - Golden Chicken - 0-Int-Grower L23 - IBF/L23 - Adnan - Finisher - Bulk',
    status: 'Approved'
  },
  {
    id: 7,
    transportationInfo:
      '	4577 VRA - Naresh Kumar - 2419805912 - Broilers - Golden Chicken - 3-Ext-Finisher E6 - IBG/L23 - Adnan - Grower - Bulk',
    status: 'Approved'
  }
];

const TransportationFeedDelivery = () => {
  const navigate = useNavigate();
  const [selectedRowIds, setSelectedRowIds] = useState([]);

  let isSelectedAll = rows.length === selectedRowIds.length;

  const handleStateChange = (state) => {
    setSelectedRowIds(state.rowSelection);
  };

  const handleRowClick = (params) => {
    const id = params.row.id;
    navigate(`/delivery/TransportationFeedDelivery/edit/${id}`);
  };

  return (
    <div>
      <div className="formula">
        <h1 className="formula__h1">Transportation Feed Delivery</h1>
        <span className="formula__link">
          <Link to={`/delivery/TransportationFeedDelivery/create`}>
            <Button variant="contained">Create Transportation Feed Delivery</Button>
          </Link>
        </span>
      </div>
      {selectedRowIds.length > 0 && (
        <div className="formula__buttons">
          <Button className="formula__buttons--button" variant="outlined" startIcon={<DeleteIcon />}>
            {isSelectedAll ? 'Delete All Items' : 'Delete Selected Items'}
          </Button>
        </div>
      )}
      <div style={{ height: 400, width: '100%' }}>
        <DataGrid
          style={{ background: '#fff' }}
          rows={rows}
          columns={columns}
          onRowClick={handleRowClick}
          onStateChange={handleStateChange}
          initialState={{
            pagination: {
              paginationModel: { page: 0, pageSize: 5 }
            }
          }}
          pageSizeOptions={[5, 10]}
          checkboxSelection
        />
      </div>
    </div>
  );
};

export default TransportationFeedDelivery;
