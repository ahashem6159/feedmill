import React from 'react';
import { Link, useParams } from 'react-router-dom';
import Button from '@mui/material/Button';
import IosShareIcon from '@mui/icons-material/IosShare';
import ArrowCircleLeftIcon from '@mui/icons-material/ArrowCircleLeft';

// Import Material Ui
import { Grid } from '@mui/material';
import CustomInput from '../../../common/CustomInput';
import deleteHandler from 'helper/DeleteAlert';
import CustomButtons from 'common/CustomButtons';

// Functions & Variables

// Component
const CreateBroilersFeedRequests = () => {
  const { id } = useParams();

  const formulaName = id ? `Edit Broilers Feed Requests / ${id}` : 'Create Broilers Feed Requests';

  const exportFormulaBtn = (
    <span className="createformula__link">
      <Button className="formula__buttons--button" variant="outlined" startIcon={<IosShareIcon />}>
        Export Formula
      </Button>
    </span>
  );

  return (
    <>
      <div className="createformula">
        <h1 className="createformula__title">{formulaName}</h1>
        <div className="createformula__links">
          <Link to={`/broilers/BroilersFeedRequests`} className="formula__Back">
            <Button className="formula__buttons--button" variant="outlined" startIcon={<ArrowCircleLeftIcon />}>
              Back
            </Button>
          </Link>
          {id && exportFormulaBtn}
        </div>
      </div>
      <div className="formula__information">
        <h3 className="createformula--heading">Request For Delivery</h3>
        <Grid container spacing={1}>
          <Grid item xs={4}>
            <div className="formula__information__parent">
              <CustomInput title="Month" type="text" />
              <CustomInput title="Farm" type="text" />
            </div>
          </Grid>
          <Grid item xs={4}>
            <div className="formula__information__details">
              <CustomInput title="Cycle" type="text" />
              <CustomInput title="Feed Type" type="text" />
            </div>
          </Grid>
          <Grid item xs={4}>
            <div className="formula__information__details">
              <CustomInput title="Quantity Requested" type="number" />
            </div>
          </Grid>
        </Grid>
      </div>
      <CustomButtons id={id} deleteHandler={deleteHandler} />
    </>
  );
};

export default CreateBroilersFeedRequests;
