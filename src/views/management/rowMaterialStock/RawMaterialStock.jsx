import React from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { DataGrid } from '@mui/x-data-grid';
import Button from '@mui/material/Button';
import DeleteIcon from '@mui/icons-material/Delete';
import { useState } from 'react';

const columns = [
  { field: 'id', headerName: 'ID', width: 70 },
  { field: 'materialName', headerName: 'Material Name', width: 130 },
  { field: 'itemCode', headerName: 'Item Code', width: 130 }
];

const rows = [
  { id: 1, materialName: 'EGC-mix', itemCode: 'EGC' },
  { id: 2, materialName: 'SOL-mix', itemCode: 'SOL' },
  { id: 3, materialName: 'Vitamin Broiler', itemCode: 'BVP' },
  { id: 4, materialName: 'Soya Bean', itemCode: 'SYB' },
  { id: 5, materialName: 'Presan FY', itemCode: 'PRF' },
  { id: 6, materialName: 'Plam Oil', itemCode: 'POL' },
  { id: 7, materialName: 'MCP', itemCode: 'MCP' },
  { id: 8, materialName: 'L-Methionine', itemCode: 'LMT' },
  { id: 9, materialName: 'L-Lysine', itemCode: 'LLY' }
];

const RawMaterialStock = () => {
  const navigate = useNavigate();
  const [selectedRowIds, setSelectedRowIds] = useState([]);

  let isSelectedAll = rows.length === selectedRowIds.length;

  const handleStateChange = (state) => {
    setSelectedRowIds(state.rowSelection);
  };

  const handleRowClick = (params) => {
    const id = params.row.id;
    navigate(`/material/RawMaterialStock/edit/${id}`);
  };

  return (
    <div>
      <div className="formula">
        <h1 className="formula__h1">Raw Material Stock</h1>
        <span className="formula__link">
          <Link to={`/material/RawMaterialStock/create`}>
            <Button variant="contained">Create Raw Material Stock</Button>
          </Link>
        </span>
      </div>
      {selectedRowIds.length > 0 && (
        <div className="formula__buttons">
          <Button className="formula__buttons--button" variant="outlined" startIcon={<DeleteIcon />}>
            {isSelectedAll ? 'Delete All Items' : 'Delete Selected Items'}
          </Button>
        </div>
      )}
      <div style={{ height: 400, width: '100%' }}>
        <DataGrid
          style={{ background: '#fff' }}
          rows={rows}
          columns={columns}
          onRowClick={handleRowClick}
          onStateChange={handleStateChange}
          initialState={{
            pagination: {
              paginationModel: { page: 0, pageSize: 5 }
            }
          }}
          pageSizeOptions={[5, 10]}
          checkboxSelection
        />
      </div>
    </div>
  );
};

export default RawMaterialStock;
