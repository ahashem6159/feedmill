import React from 'react';
import { Link, useParams } from 'react-router-dom';
import Button from '@mui/material/Button';
import ArrowCircleLeftIcon from '@mui/icons-material/ArrowCircleLeft';

import { Grid } from '@mui/material';
import CustomInput from 'common/CustomInput';
import CustomButton from 'common/CustomButton';

// Functions & Variables
import deleteHandler from 'helper/DeleteAlert';

// Component
const CreateRawMaterialStock = () => {
  const { id } = useParams();

  const formulaName = id ? `Edit Raw Material Stock / ${id}` : 'Create Raw Material Stock';

  return (
    <>
      <div className="createformula">
        <h1 className="createformula__title">{formulaName}</h1>
        <div className="createformula__links">
          <Link to={`/material/RawMaterialStock`} className="formula__Back">
            <Button className="formula__buttons--button" variant="outlined" startIcon={<ArrowCircleLeftIcon />}>
              Back
            </Button>
          </Link>
        </div>
      </div>
      <div className="formula__information">
        <h3 className="createformula--heading">Change Raw Material Stock</h3>
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <div className="formula__information__details">
              <CustomInput title="Material Name" type="text" />
              <CustomInput title="Item Code" type="text" />
            </div>
          </Grid>
        </Grid>
      </div>
      <div className="customButtons">
        <CustomButton title="Save" />
        {id && <CustomButton color="error" title="Delete" onClick={deleteHandler} />}
      </div>
    </>
  );
};

export default CreateRawMaterialStock;
