import React from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { DataGrid } from '@mui/x-data-grid';
import Button from '@mui/material/Button';
import DeleteIcon from '@mui/icons-material/Delete';
import { useState } from 'react';

const columns = [
  { field: 'id', headerName: 'ID', width: 70 },
  { field: 'selectedFarm', headerName: 'Selected Farm', width: 130 },
  { field: 'feedType', headerName: 'Feed Type', width: 130 },
  {
    field: 'quantity',
    headerName: 'Quantity In Tons',
    width: 130
  },
  {
    field: 'batchNumber',
    headerName: 'Batch Number',
    description: 'This column has a value getter and is not sortable.',
    width: 130
  },
  {
    field: 'production',
    headerName: 'Production',
    width: 120
  },
  {
    field: 'totalPrice',
    headerName: 'Total Price',
    width: 120
  },
  {
    field: 'formula',
    headerName: 'Formula',
    width: 120
  },
  {
    field: 'numberOfDelivery',
    headerName: 'Number Of Delivery',
    width: 120
  },
  {
    field: 'supplier',
    headerName: 'Supplier',
    width: 120
  },
  {
    field: 'transportationDate',
    headerName: 'Transportation Date',
    width: 120
  },
  {
    field: 'transportationTime',
    headerName: 'Transportation Time',
    width: 120
  },
  {
    field: 'deliveryDate',
    headerName: 'Delivery Date',
    width: 120
  },
  {
    field: 'deliveryTime',
    headerName: 'Delivery Time',
    width: 120
  },
  {
    field: 'deliveryTo',
    headerName: 'Delivery To',
    width: 120
  },
  {
    field: 'truck',
    headerName: 'Truck',
    width: 120
  },
  {
    field: 'driver',
    headerName: 'Driver',
    width: 120
  },
  {
    field: 'matched',
    headerName: 'Matched',
    width: 120
  }
];

const rows = [
  {
    id: 1,
    selectedFarm: 'sara',
    feedType: 'Starter',
    quantity: 24.7,
    batchNumber: 'IBNone/2023-12-07/ IBS/L23/4407/6/16	',
    production: '-',
    totalPrice: 0.0,
    formula: '3-Ext-Starter E6 - IBS/L23',
    numberOfDelivery: 'Bulk',
    supplier: 'Golden Chicken',
    transportationDate: 'Dec. 7, 2023',
    transportationTime: '2:39 a.m',
    deliveryDate: '	Dec. 8, 2023',
    deliveryTime: '5:06 a.m',
    deliveryTo: 'Broilers',
    truck: '1772 ARA',
    driver: 'Nirmal Singh - 2402140509',
    matched: '-'
  }
];

const Transportation = () => {
  const navigate = useNavigate();
  const [selectedRowIds, setSelectedRowIds] = useState([]);

  let isSelectedAll = rows.length === selectedRowIds.length;

  const handleStateChange = (state) => {
    setSelectedRowIds(state.rowSelection);
  };

  const handleRowClick = (params) => {
    const id = params.row.id;
    navigate(`/administration/Transportation/edit/${id}`);
  };

  return (
    <div>
      <div className="formula">
        <h1 className="formula__h1">Transportation</h1>
        <span className="formula__link">
          <Link to={`/administration/Transportation/create`}>
            <Button variant="contained">Create Transportation</Button>
          </Link>
        </span>
      </div>
      {selectedRowIds.length > 0 && (
        <div className="formula__buttons">
          <Button className="formula__buttons--button" variant="outlined" startIcon={<DeleteIcon />}>
            {isSelectedAll ? 'Delete All Items' : 'Delete Selected Items'}
          </Button>
        </div>
      )}
      <div style={{ height: 400, width: '100%' }}>
        <DataGrid
          style={{ background: '#fff' }}
          rows={rows}
          columns={columns}
          onRowClick={handleRowClick}
          onStateChange={handleStateChange}
          initialState={{
            pagination: {
              paginationModel: { page: 0, pageSize: 5 }
            }
          }}
          pageSizeOptions={[5, 10]}
          checkboxSelection
        />
      </div>
    </div>
  );
};

export default Transportation;
