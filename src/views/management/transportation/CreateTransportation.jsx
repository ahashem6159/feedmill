import React from 'react';
import { Link, useParams } from 'react-router-dom';
import Button from '@mui/material/Button';
import ArrowCircleLeftIcon from '@mui/icons-material/ArrowCircleLeft';

// Import Material Ui
import { useState } from 'react';
import { Grid } from '@mui/material';
import deleteHandler from 'helper/DeleteAlert';
import CustomSelectSearch from 'common/CustomSelectSearch';
import CustomInput from 'common/CustomInput';
import RawMaterial from '../formula/RawMaterial';
import CustomDataPicker from 'common/CustomDataPicker';
import CustomTimePicker from 'common/CustomTimePicker';
import CustomButtons from 'common/CustomButtons';

// Functions & Variables
const supplierData = ['Golden Chicken', 'Taya Feed Mill', 'Arasco Feed Mill'];
const DeliverData = ['Broilers', 'External Broilers', 'Breeders', 'External Breeders'];

const topRawData = [
  'Raw Material 1 - YLC - QTY Left: 0.0',
  'Raw Material 2 - 110142 - QTY Left: 2552538.62',
  'Raw Material 3 - 110187 - QTY Left: -115471.68',
  'Raw Material 4 - 110122 - QTY Left: 2037439.06',
  'Raw Material 5 - 110131 - QTY Left: 798931.52',
  'Raw Material 6 - 110171 - QTY Left: 1982495.87',
  'Raw Material 7 - 110136 - QTY Left: 1935311.7',
  'Raw Material 8 - 110129 - QTY Left: 357.77'
];

// Component
const CreateTransportation = () => {
  const { id } = useParams();
  const [selectedValue, setSelectedValue] = useState(null);

  const handleAutocompleteChange = (event, newValue) => {
    console.log(newValue);
    setSelectedValue('');
  };

  const formulaName = id ? `Edit Transportation / ${id}` : 'Create Transportation';

  return (
    <>
      <div className="createformula">
        <h1 className="createformula__title">{formulaName}</h1>
        <div className="createformula__links">
          <Link to={`/administration/Transportation`} className="formula__Back">
            <Button className="formula__buttons--button" variant="outlined" startIcon={<ArrowCircleLeftIcon />}>
              Back
            </Button>
          </Link>
        </div>
      </div>
      <div className="formula__information">
        <h3 className="createformula--heading">Delivery Information</h3>
        <Grid container spacing={0}>
          <Grid item xs={4}>
            <div className="formula__information__details">
              <div className="formula__information__parent__type">
                <p className="date">Transportation Date</p>
                <CustomDataPicker />
              </div>
              <div className="formula__information__parent__type">
                <p className="date">Transportation Time</p>
                <CustomTimePicker />
              </div>
              <CustomSelectSearch title="Driver" data={supplierData} value={selectedValue} onChange={handleAutocompleteChange} />
            </div>
          </Grid>
          <Grid item xs={4}>
            <div className="formula__information__details">
              <CustomSelectSearch title="To Broilers Farm" data={DeliverData} value={selectedValue} onChange={handleAutocompleteChange} />
              <CustomSelectSearch title="Deliver To" data={supplierData} value={selectedValue} onChange={handleAutocompleteChange} />
            </div>
          </Grid>
          <Grid item xs={4}>
            <div className="formula__information__details">
              <CustomSelectSearch title="Feed Type" data={supplierData} value={selectedValue} onChange={handleAutocompleteChange} />
              <CustomSelectSearch title="Truck" data={supplierData} value={selectedValue} onChange={handleAutocompleteChange} />
            </div>
          </Grid>
          <Grid item xs={4}>
            <div className="formula__information__details"></div>
          </Grid>
        </Grid>
      </div>
      <div className="formula__information">
        <h3 className="createformula--heading">Production Information</h3>
        <Grid container spacing={0}>
          <Grid item xs={4}>
            <div className="formula__information__details">
              <div className="formula__information__parent__type">
                <p className="date">Production Date</p>
                <CustomDataPicker />
              </div>
              <div className="formula__information__parent__type">
                <p className="date">Delivery Date</p>
                <CustomDataPicker />
              </div>
            </div>
          </Grid>
          <Grid item xs={4}>
            <div className="formula__information__details">
              <CustomSelectSearch title="Formula" data={supplierData} value={selectedValue} onChange={handleAutocompleteChange} />
              <CustomSelectSearch
                title="Manner of Delivery"
                data={supplierData}
                value={selectedValue}
                onChange={handleAutocompleteChange}
              />
            </div>
          </Grid>
          <Grid item xs={4}>
            <div className="formula__information__details">
              <div className="formula__information__parent__type">
                <p className="date">Delivery Time</p>
                <CustomTimePicker />
              </div>
            </div>
          </Grid>
        </Grid>
      </div>
      <div className="formula__information">
        <h3 className="createformula--heading">Transportation Information</h3>
        <Grid container spacing={0}>
          <Grid item xs={12}>
            <div className="formula__information__details">
              <div style={{ display: 'flex' }}>
                <CustomInput title="Batch Number" type="text" />
                <CustomInput title="Quantity in Tons" type="number" />
                <CustomInput title="Total Price" type="number" />
              </div>
            </div>
          </Grid>
        </Grid>
      </div>
      <RawMaterial headerTitle="Transportation Raw Material" data={topRawData} />
      <CustomButtons id={id} deleteHandler={deleteHandler} />
    </>
  );
};

export default CreateTransportation;
