import React from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { DataGrid } from '@mui/x-data-grid';
import Button from '@mui/material/Button';
import DeleteIcon from '@mui/icons-material/Delete';
import { useState } from 'react';

const columns = [
  { field: 'id', headerName: 'ID', width: 70 },
  { field: 'materialName', headerName: 'Material Name', width: 130 },
  { field: 'itemCode', headerName: 'Item Code', width: 130 },
  {
    field: 'openStockInKgs',
    headerName: 'Open Stock In KGS',
    width: 160
  },
  {
    field: 'quantityConsumedInKGs',
    headerName: 'Quantity Consumed in KGs',
    description: 'This column has a value getter and is not sortable.',
    sortable: false,
    width: 170
  },
  {
    field: 'addedValueInKGs',
    headerName: 'Added Value in KGs',
    description: 'This column has a value getter and is not sortable.',
    sortable: false,
    width: 160
  }
];

const rows = [
  { id: 1, materialName: 'Raw Material 85', itemCode: 'EGC', openStockInKgs: 35, quantityConsumedInKGs: 3009.14, addedValueInKGs: 22 },
  { id: 2, materialName: 'Raw Material 84', itemCode: 'SOL', openStockInKgs: 42, quantityConsumedInKGs: 1806.45, addedValueInKGs: 31 },
  { id: 3, materialName: 'Raw Material 84', itemCode: 'BVP', openStockInKgs: 45, quantityConsumedInKGs: 420.79, addedValueInKGs: 13 },
  { id: 4, materialName: 'Raw Material 83', itemCode: 'SYB', openStockInKgs: 16, quantityConsumedInKGs: 2168.23, addedValueInKGs: 73 },
  { id: 5, materialName: 'Raw Material 82', itemCode: 'PRF', openStockInKgs: 20, quantityConsumedInKGs: 130.13, addedValueInKGs: 93 },
  { id: 6, materialName: 'Raw Material 81', itemCode: 'POL', openStockInKgs: 150, quantityConsumedInKGs: 20638.25, addedValueInKGs: 10 },
  { id: 7, materialName: 'Raw Material 80', itemCode: 'MCP', openStockInKgs: 44, quantityConsumedInKGs: 0.0, addedValueInKGs: 83 }
];

const MaterialStock = () => {
  const navigate = useNavigate();
  const [selectedRowIds, setSelectedRowIds] = useState([]);

  let isSelectedAll = rows.length === selectedRowIds.length;

  const handleStateChange = (state) => {
    setSelectedRowIds(state.rowSelection);
  };

  const handleRowClick = (params) => {
    const id = params.row.id;
    navigate(`/material/Material-Stock/edit/${id}`);
  };

  return (
    <div>
      <div className="formula">
        <h1 className="formula__h1">Material Stock</h1>
        <span className="formula__link">
          <Link to={`/material/Material-Stock/create`}>
            <Button variant="contained">Create Stock</Button>
          </Link>
        </span>
      </div>
      {selectedRowIds.length > 0 && (
        <div className="formula__buttons">
          <Button className="formula__buttons--button" variant="outlined" startIcon={<DeleteIcon />}>
            {isSelectedAll ? 'Delete All Items' : 'Delete Selected Items'}
          </Button>
        </div>
      )}
      <div style={{ height: 400, width: '100%' }}>
        <DataGrid
          style={{ background: '#fff' }}
          rows={rows}
          columns={columns}
          onRowClick={handleRowClick}
          onStateChange={handleStateChange}
          initialState={{
            pagination: {
              paginationModel: { page: 0, pageSize: 5 }
            }
          }}
          pageSizeOptions={[5, 10]}
          checkboxSelection
        />
      </div>
    </div>
  );
};

export default MaterialStock;
