import React from 'react';
import { Link, useParams } from 'react-router-dom';
import Button from '@mui/material/Button';
import ArrowCircleLeftIcon from '@mui/icons-material/ArrowCircleLeft';

import { useState } from 'react';
import { Grid } from '@mui/material';
import CustomInput from 'common/CustomInput';

// Functions & Variables
import deleteHandler from 'helper/DeleteAlert';
import CustomButtons from 'common/CustomButtons';
import CustomSelectSearch from 'common/CustomSelectSearch';

const names = [
  'Oliver Hansen',
  'Van Henry',
  'April Tucker',
  'Ralph Hubbard',
  'Omar Alexander',
  'Carlos Abbott',
  'Miriam Wagner',
  'Bradley Wilkerson',
  'Virginia Andrews',
  'Kelly Snyder'
];

// Component
const CreateStock = () => {
  const { id } = useParams();
  const [selectedValue, setSelectedValue] = useState(null);

  const handleAutocompleteChange = (event, newValue) => {
    console.log(newValue);
    setSelectedValue('');
  };

  const formulaName = id ? `Edit Stock / ${id}` : 'Create Stock';

  return (
    <>
      <div className="createformula">
        <h1 className="createformula__title">{formulaName}</h1>
        <div className="createformula__links">
          <Link to={`/material/Material-Stock`} className="formula__Back">
            <Button className="formula__buttons--button" variant="outlined" startIcon={<ArrowCircleLeftIcon />}>
              Back
            </Button>
          </Link>
        </div>
      </div>
      <div className="formula__information">
        <h3 className="createformula--heading">Change Raw Material Stock</h3>
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <div className="formula__information__details">
              <CustomInput title="Material Name" type="text" />
              <CustomInput title="Item Code" type="text" />
              <CustomSelectSearch title="Feed Type" data={names} value={selectedValue} onChange={handleAutocompleteChange} />
            </div>
          </Grid>
        </Grid>
      </div>
      <CustomButtons id={id} deleteHandler={deleteHandler} />
    </>
  );
};

export default CreateStock;
