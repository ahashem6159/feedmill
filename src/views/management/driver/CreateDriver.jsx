import React from 'react';
import { Link, useParams } from 'react-router-dom';
import Button from '@mui/material/Button';
import ArrowCircleLeftIcon from '@mui/icons-material/ArrowCircleLeft';

import { Grid } from '@mui/material';

// Import Components
import CustomInput from 'common/CustomInput';
import CustomButtons from 'common/CustomButtons';

// Helper Functions & Variables
import deleteHandler from 'helper/DeleteAlert';

// Component
const CreateDriver = () => {
  const { id } = useParams();

  const formulaName = id ? `Edit Driver / ${id}` : 'Create Driver';

  return (
    <>
      <div className="createformula">
        <h1 className="createformula__title">{formulaName}</h1>
        <div className="createformula__links">
          <Link to={`/administration/Drivers`} className="formula__Back">
            <Button className="formula__buttons--button" variant="outlined" startIcon={<ArrowCircleLeftIcon />}>
              Back
            </Button>
          </Link>
        </div>
      </div>
      <div className="formula__information">
        <h3 className="createformula--heading">Driver</h3>
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <div className="formula__information__details">
              <CustomInput title="Driver Name" type="text" />
              <CustomInput title="License" type="number" />
            </div>
          </Grid>
        </Grid>
      </div>
      <CustomButtons id={id} deleteHandler={deleteHandler} />
    </>
  );
};

export default CreateDriver;
