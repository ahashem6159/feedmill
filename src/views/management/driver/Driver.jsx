import React from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { DataGrid } from '@mui/x-data-grid';
import Button from '@mui/material/Button';
import DeleteIcon from '@mui/icons-material/Delete';
import { useState } from 'react';

const columns = [
  { field: 'id', headerName: 'ID', width: 70 },
  { field: 'driverName', headerName: 'Driver Name', width: 130 },
  { field: 'license', headerName: 'License', width: 130 }
];

const rows = [
  { id: 1, driverName: 'Mohamed Sirfraz', license: 2355632106 },
  { id: 2, driverName: 'Abdelrazik Khan', license: 2389969912 },
  { id: 3, driverName: 'Al Nada driver', license: 33111 },
  { id: 4, driverName: 'Nirmal Singh', license: 2402140509 }
];

const Driver = () => {
  const navigate = useNavigate();
  const [selectedRowIds, setSelectedRowIds] = useState([]);

  let isSelectedAll = rows.length === selectedRowIds.length;

  const handleStateChange = (state) => {
    setSelectedRowIds(state.rowSelection);
  };

  const handleRowClick = (params) => {
    const id = params.row.id;
    navigate(`/administration/Drivers/edit/${id}`);
  };

  return (
    <div>
      <div className="formula">
        <h1 className="formula__h1">Drivers</h1>
        <span className="formula__link">
          <Link to={`/administration/Drivers/create`}>
            <Button variant="contained">Create Driver</Button>
          </Link>
        </span>
      </div>
      {selectedRowIds.length > 0 && (
        <div className="formula__buttons">
          <Button className="formula__buttons--button" variant="outlined" startIcon={<DeleteIcon />}>
            {isSelectedAll ? 'Delete All Items' : 'Delete Selected Items'}
          </Button>
        </div>
      )}
      <div style={{ height: 400, width: '100%' }}>
        <DataGrid
          style={{ background: '#fff' }}
          rows={rows}
          columns={columns}
          onRowClick={handleRowClick}
          onStateChange={handleStateChange}
          initialState={{
            pagination: {
              paginationModel: { page: 0, pageSize: 5 }
            }
          }}
          pageSizeOptions={[5, 10]}
          checkboxSelection
        />
      </div>
    </div>
  );
};

export default Driver;
