import React from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { DataGrid } from '@mui/x-data-grid';
import Button from '@mui/material/Button';
import DeleteIcon from '@mui/icons-material/Delete';
import { useState } from 'react';
import CustomLoading from 'common/CustomLoading';
import PageNotFound from 'views/pages/authentication/PageNotFound';
import useFetchDataFormula from 'hooks/useFetchDataFormula';
import { Grid } from '@mui/material';
import GroupsIcon from '@mui/icons-material/Groups';
import CustomSelectSearch from 'common/CustomSelectSearch';

const top100Films = ['The Shawshank Redemption', 'The Dark Knight', 'The Godfather'];

const columns = [
  // { field: 'id', headerName: 'ID', width: 70 },
  { field: 'FormulaName', headerName: 'Formula Name', width: 160 },
  { field: 'code', headerName: 'Code', width: 130 },
  {
    field: 'FormulaType',
    headerName: 'Formula Type',
    width: 160
  },
  {
    field: 'FeedType',
    headerName: 'Feed Type',
    width: 160
  },
  {
    field: 'Status',
    headerName: 'Status',
    width: 160,
    renderCell: (params) => <span className={params.value === 'Active' ? 'statusActive' : 'statusInactive'}>{params.value}</span>
  }
];

const isError = (error) => {
  return error && typeof error === 'string' && ['404', '500', 'Network Error'].some((err) => error.includes(err));
};

const Card = ({ header, title, grid, number, type, total }) => (
  <Grid item xs={12} md={grid}>
    <div className="cardFormula">
      <div className="cardFormula__content">
        <h5 className="cardFormula__header">{header}</h5>
        <div className="cardFormula__main">
          <p className="cardFormula__main__title">
            {title}
            <span className="cardFormula__main__numbers">
              {number}
              <span className="cardFormula__main__type">
                ({type} {header === 'Formulas' && 'types'})
              </span>
            </span>
          </p>
          <p className="cardFormula__footer">{total}</p>
        </div>
      </div>
      {/* {icon && <div className="cardFormula__icon">{icon}</div>} */}
    </div>
  </Grid>
);

const Formula = () => {
  const navigate = useNavigate();
  const [selectedRowIds, setSelectedRowIds] = useState([]);
  const { dataFormula, isLoading, error } = useFetchDataFormula();
  const [selectedValue, setSelectedValue] = useState(null);

  const handleAutocompleteChange = (event, newValue) => {
    setSelectedValue(newValue);
  };

  const handleStateChange = (state) => {
    setSelectedRowIds(state.rowSelection);
  };

  const handleRowClick = (params) => {
    navigate(`/administration/Formula/edit/${params.row.id}`);
  };

  if (isLoading) return <CustomLoading />;
  if (isError(error)) return <PageNotFound />;

  const rows = dataFormula?.data?.map((item) => ({
    id: item.id,
    FormulaName: item.name,
    code: item.code,
    FormulaType: item.formula_type,
    FeedType: item.formula_type,
    // Status: item.status,
    Status: item.status ? 'Active' : 'Inactive'
  }));

  let isSelectedAll = rows?.length === selectedRowIds.length;

  console.log(dataFormula);

  return (
    <div>
      <div className="formula__card">
        <Grid container spacing={2}>
          <Card grid="3" header="Formulas" icon={<GroupsIcon />} number="26" type="8" total="Total formulas" />
          <Card grid="3" header="Active Formulas" icon={<GroupsIcon />} number="17" type="80%" total="Last added 3 days ago" />
          <Card grid="3" header="Inactive formulas" icon={<GroupsIcon />} number="9" type="20%" total="Deactivated since 3 days ago" />
          <Card grid="3" header="Internal Formulas" icon={<GroupsIcon />} number="23" type="3 External" total="Internal and External" />
        </Grid>
      </div>
      <div className="formula">
        <Grid container spacing={2}>
          <Grid item xs={3.5}>
            {/* <h1 className="formula__h1">Formula</h1> */}
            <CustomSelectSearch label="Feed Type" data={top100Films} value={selectedValue} onChange={handleAutocompleteChange} />
          </Grid>
          <Grid item xs={3.5}>
            {/* <h1 className="formula__h1">Formula</h1> */}
            <CustomSelectSearch label="Formula Type" data={top100Films} value={selectedValue} onChange={handleAutocompleteChange} />
          </Grid>
          <Grid item xs={3.5}>
            {/* <h1 className="formula__h1">Formula</h1> */}
            <CustomSelectSearch label="Status" data={top100Films} value={selectedValue} onChange={handleAutocompleteChange} />
          </Grid>
          <Grid item xs={1.5} sx={{ display: 'flex', alignItems: 'center' }}>
            <span className="formula__link">
              <Link to={`/administration/Formula/create`}>
                <Button variant="contained">Create Formula</Button>
              </Link>
            </span>
          </Grid>
        </Grid>
      </div>
      {selectedRowIds.length > 0 && (
        <div className="formula__buttons">
          <Button className="formula__buttons--button" variant="outlined" startIcon={<DeleteIcon />}>
            {isSelectedAll ? 'Delete All Items' : 'Delete Selected Items'}
          </Button>
        </div>
      )}
      <div style={{ height: 400, width: '100%' }}>
        <DataGrid
          style={{ background: '#fff' }}
          rows={rows ?? []}
          columns={columns}
          onRowClick={handleRowClick}
          onStateChange={handleStateChange}
          initialState={{
            pagination: {
              paginationModel: { page: 0, pageSize: 5 }
            },
            sorting: false
          }}
          pageSizeOptions={[5, 10]}
          checkboxSelection
        />
      </div>
    </div>
  );
};

export default Formula;
