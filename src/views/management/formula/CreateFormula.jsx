import React from 'react';
import { Link, useParams } from 'react-router-dom';
import Button from '@mui/material/Button';
import IosShareIcon from '@mui/icons-material/IosShare';
import ArrowCircleLeftIcon from '@mui/icons-material/ArrowCircleLeft';

import { Grid } from '@mui/material';
import NutrientsInformation from './NutrientsInformation';
import CustomInput from '../../../common/CustomInput';
import RawMaterial from './RawMaterial';
import deleteHandler from 'helper/DeleteAlert';
import CustomSelectSearch from 'common/CustomSelectSearch';
import { useState } from 'react';
import CustomButtons from 'common/CustomButtons';
import CustomLoading from 'common/CustomLoading';
import PageNotFound from 'views/pages/authentication/PageNotFound';
import { getFormula } from 'slices/formulaSlice';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux';

const top100Films = [
  'Raw Material 1 - YLC - QTY Left: 0.0',
  'Raw Material 2 - 110142 - QTY Left: 2552538.62',
  'Raw Material 3 - 110187 - QTY Left: -115471.68',
  'Raw Material 4 - 110122 - QTY Left: 2037439.06',
  'Raw Material 5 - 110131 - QTY Left: 798931.52',
  'Raw Material 6 - 110171 - QTY Left: 1982495.87',
  'Raw Material 7 - 110136 - QTY Left: 1935311.7',
  'Raw Material 8 - 110129 - QTY Left: 357.77'
];
const isError = (error) => {
  return error && typeof error === 'string' && ['404', '500', 'Network Error'].some((err) => error.includes(err));
};

// Component
const CreateFormula = () => {
  const dispatch = useDispatch();
  const { id } = useParams();
  const [selectedValue, setSelectedValue] = useState(null);
  const { currentFormula, isLoading, error } = useSelector((state) => state.formula);

  console.log(currentFormula);

  useEffect(() => {
    if (id) dispatch(getFormula(id));
  }, [id]);

  const handleAutocompleteChange = (event, newValue) => {
    setSelectedValue(newValue);
  };

  const formulaName = id ? `Edit Formula / ${id}` : 'Create Formula';

  if (isLoading) return <CustomLoading />;
  if (isError(error)) return <PageNotFound />;

  const exportFormulaBtn = (
    <span className="createformula__link">
      <Button className="formula__buttons--button" variant="outlined" startIcon={<IosShareIcon />}>
        Export Formula
      </Button>
    </span>
  );

  return (
    <>
      <div className="createformula">
        <h1 className="createformula__title">{formulaName}</h1>
        <div className="createformula__links">
          <Link to={`/administration/formula`} className="formula__Back">
            <Button className="formula__buttons--button" variant="outlined" startIcon={<ArrowCircleLeftIcon />}>
              Back
            </Button>
          </Link>
          {id && exportFormulaBtn}
        </div>
      </div>
      {/* Content Formula */}
      <div className="formula__information">
        <h3 className="createformula--heading">Formula Information</h3>
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <div className="formula__information__parent">
              <CustomSelectSearch title="Parent Formula" data={top100Films} value={selectedValue} onChange={handleAutocompleteChange} />
              <CustomSelectSearch title="Formula Type" data={top100Films} value={selectedValue} onChange={handleAutocompleteChange} />
            </div>
          </Grid>
          <Grid item xs={12}>
            <div className="formula__information__details">
              <CustomInput title="Formula Name" type="text" />
              <CustomInput title="Wheight in tons" type="text" />
            </div>
          </Grid>
          <Grid item xs={12}>
            <div className="formula__information__details">
              <CustomSelectSearch title="Feed Type" data={top100Films} value={selectedValue} onChange={handleAutocompleteChange} />
              <CustomInput title="Code" type="text" />
            </div>
          </Grid>
        </Grid>
      </div>
      <NutrientsInformation />
      <RawMaterial headerTitle="Formula Raw Material" data={top100Films} />
      <CustomButtons id={id} deleteHandler={deleteHandler} />
    </>
  );
};

export default CreateFormula;
