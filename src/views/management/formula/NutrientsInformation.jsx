import React, { useState } from 'react';
import { Grid } from '@mui/material';

import CustomInput from '../../../common/CustomInput';

const NutrientsInformation = () => {
  const [energy, setEnergy] = useState('');
  const [protein, setProtein] = useState('');

  return (
    <>
      <div className="formula__information">
        <h3 className="createformula--heading">Nutrients Information</h3>
        <Grid container spacing={1} sx={{ width: '86%', margin: 'auto' }}>
          <Grid item xs={4}>
            <div className="formula__information__parent Nutrients">
              <CustomInput title="Energy" onChange={(e) => setEnergy(e.target.value)} value={energy} type="number" />
              <CustomInput title="Protein" onChange={(e) => setProtein(e.target.value)} value={protein} type="number" />
            </div>
          </Grid>
          <Grid item xs={4}>
            <div className="formula__information__parent Nutrients">
              <CustomInput title="Calcium" onChange={(e) => setEnergy(e.target.value)} value={energy} type="number" />
              <CustomInput title="Phosphorus" onChange={(e) => setProtein(e.target.value)} value={protein} type="number" />
            </div>
          </Grid>
          <Grid item xs={4}>
            <div className="formula__information__parent Nutrients">
              <CustomInput title="Met" onChange={(e) => setEnergy(e.target.value)} value={energy} type="number" />
              <CustomInput title="Met + Cys" onChange={(e) => setProtein(e.target.value)} value={protein} type="number" />
            </div>
          </Grid>
          <Grid item xs={4}>
            <div className="formula__information__parent Nutrients">
              <CustomInput title="Lys" onChange={(e) => setEnergy(e.target.value)} value={energy} type="number" />
              <CustomInput title="Fat" onChange={(e) => setProtein(e.target.value)} value={protein} type="number" />
            </div>
          </Grid>
          <Grid item xs={4}>
            <div className="formula__information__parent Nutrients">
              <CustomInput title="Moisture" onChange={(e) => setEnergy(e.target.value)} value={energy} type="number" />
              <CustomInput title="Fibre" onChange={(e) => setProtein(e.target.value)} value={protein} type="number" />
            </div>
          </Grid>
          <Grid item xs={4}>
            <div className="formula__information__parent Nutrients">
              <CustomInput title="Ash" onChange={(e) => setEnergy(e.target.value)} value={energy} type="number" />
              <CustomInput title="Starch" onChange={(e) => setProtein(e.target.value)} value={protein} type="number" />
            </div>
          </Grid>
        </Grid>
      </div>
    </>
  );
};

export default NutrientsInformation;
