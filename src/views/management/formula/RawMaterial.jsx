import React, { useState } from 'react';
import { Button, Grid } from '@mui/material';

import CustomRaw from 'common/CustomRaw';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';

const RawMaterial = ({ headerTitle, data }) => {
  const [rawMaterialArray, setRawMaterialArray] = useState([{ id: 1, value: null, quantity: 1 }]);

  const addRawHandler = () => {
    let newRaw = {
      id: Math.random() * 0.2,
      value: null,
      quantity: 1
    };

    setRawMaterialArray((prevSate) => [...prevSate, newRaw]);
  };

  return (
    <>
      <div className="formula__information">
        <h3 className="createformula--heading">{headerTitle}</h3>
        <Grid container spacing={0}>
          {rawMaterialArray.length > 0 &&
            rawMaterialArray.map((item) => (
              <CustomRaw headerTitle={headerTitle} key={item.id} item={item} setRawMaterialArray={setRawMaterialArray} data={data} />
            ))}
          <div className="addBtn">
            <Button className="addBtn--add" variant="outlined" endIcon={<AddCircleOutlineIcon />} onClick={addRawHandler}>
              Add New Raw
            </Button>
          </div>
        </Grid>
      </div>
    </>
  );
};

export default RawMaterial;
