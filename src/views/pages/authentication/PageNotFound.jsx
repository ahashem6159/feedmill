import React from 'react';
import './pageNotFound.css';
import { Link } from 'react-router-dom';
import ErrorImg from '../../../assets/images/404.svg';

const PageNotFound = () => {
  return (
    <>
      <div className="errorPage">
        <img src={ErrorImg} alt="Error" className="img" />
        <div className="wrapper">
          <h1>Page Not Found</h1>
          {/* <p className="message">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Id sit inventore eveniet cumque alias.</p> */}
          <Link to="/" className="btn">
            Back Home
          </Link>
        </div>
      </div>
    </>
  );
};

export default PageNotFound;
