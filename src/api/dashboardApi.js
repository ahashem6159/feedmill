// const currentLocation = window.location.hostname;

// const BASE_URL = `https://${currentLocation}/FeedMale/home-page/`;
export const BASE_URL = 'https://testserver2.abuerdan.com/FeedMale/home-page/';
// export const BASE_URL = 'http://192.168.1.125:8000/FeedMale/home-page/';
// // const BASE_URL = 'http://192.168.1.111:8000/FeedMale/home-page/';
export function constructDataFetchUrl({ startDate = '', endDate = '' }) {
  const queryParams = new URLSearchParams();
  if (startDate) queryParams.append('start_at', startDate);
  if (endDate) queryParams.append('end_at', endDate);
  return `${BASE_URL}?${queryParams}`;
}
