// assets
// import { IconTypography, IconPalette, IconShadow, IconWindmill } from '@tabler/icons';
// import FlutterDashIcon from '@mui/icons-material/FlutterDash';
// import DeliveryDiningIcon from '@mui/icons-material/DeliveryDining';
// import Inventory2OutlinedIcon from '@mui/icons-material/Inventory2Outlined';
// import AssessmentOutlinedIcon from '@mui/icons-material/AssessmentOutlined';
// import LocalShippingOutlinedIcon from '@mui/icons-material/LocalShippingOutlined';

// constant
// const icons = {
//   IconTypography,
//   IconPalette,
//   IconShadow,
//   IconWindmill,
//   FlutterDashIcon,
//   DeliveryDiningIcon,
//   AssessmentOutlinedIcon,
//   Inventory2OutlinedIcon,
//   LocalShippingOutlinedIcon
// };

// ==============================|| UTILITIES MENU ITEMS ||============================== //

const utilities = {
  id: 'management',
  // title: 'Management',
  // type: 'group',
  children: [
    // {
    //   id: 'administration',
    //   title: 'administration',
    //   type: 'item',
    //   url: '/utils/util-typography',
    //   icon: icons.IconWindmill,
    //   breadcrumbs: false
    // }
    // {
    //   id: 'administration',
    //   title: 'Administration',
    //   type: 'collapse',
    //   icon: icons.IconWindmill,
    //   children: [
    //     {
    //       id: 'formula',
    //       title: 'Formula',
    //       type: 'item',
    //       url: '/administration/formula',
    //       breadcrumbs: false
    //     },
    //     {
    //       id: 'Transportation',
    //       title: 'Transportation',
    //       type: 'item',
    //       url: '/administration/Transportation',
    //       breadcrumbs: false
    //     },
    //     {
    //       id: 'Production',
    //       title: 'Production',
    //       type: 'item',
    //       url: '/administration/Production',
    //       breadcrumbs: false
    //     },
    //     {
    //       id: 'Drivers',
    //       title: 'Drivers',
    //       type: 'item',
    //       url: '/administration/Drivers',
    //       breadcrumbs: false
    //     },
    //     {
    //       id: 'MedicationInfo',
    //       title: 'Medication Info',
    //       type: 'item',
    //       url: '/administration/MedicationInfo',
    //       breadcrumbs: false
    //     }
    //   ]
    // }
    // {
    //   id: 'reports',
    //   title: 'Reports',
    //   type: 'collapse',
    //   icon: icons.AssessmentOutlinedIcon,
    //   children: [
    //     {
    //       id: 'FeedSupplier',
    //       title: 'Feed Supplier',
    //       type: 'item',
    //       url: '/reports/FeedSupplier',
    //       breadcrumbs: false
    //     },
    //     {
    //       id: 'FeedType',
    //       title: 'Feed Type',
    //       type: 'item',
    //       url: '/reports/FeedType',
    //       breadcrumbs: false
    //     },
    //     {
    //       id: 'Reason',
    //       title: 'Reason',
    //       type: 'item',
    //       url: '/reports/Reason',
    //       breadcrumbs: false
    //     },
    //     {
    //       id: 'Truck',
    //       title: 'Truck',
    //       type: 'item',
    //       url: '/reports/Truck',
    //       breadcrumbs: false
    //     }
    //   ]
    // },
    // {
    //   id: 'breeders',
    //   title: 'Breeders / Broilers',
    //   type: 'collapse',
    //   icon: icons.FlutterDashIcon,
    //   children: [
    //     {
    //       id: 'BreedersExternalFarm',
    //       title: 'Breeders External Farm',
    //       type: 'item',
    //       url: '/breeders/BreedersExternalFarm',
    //       breadcrumbs: false
    //     },
    //     {
    //       id: 'BroilersExternalFarm',
    //       title: 'Broilers External Farm',
    //       type: 'item',
    //       url: '/broilers/BroilersExternalFarm',
    //       breadcrumbs: false
    //     },
    //     {
    //       id: 'BreedersFeedRequests',
    //       title: 'Breeders Feed',
    //       type: 'item',
    //       url: '/breeders/BreedersFeedRequests',
    //       breadcrumbs: false
    //     },
    //     {
    //       id: 'BroilersFeedRequests',
    //       title: 'Broilers Feed',
    //       type: 'item',
    //       url: '/broilers/BroilersFeedRequests',
    //       breadcrumbs: false
    //     }
    //   ]
    // },
    // {
    //   id: 'material',
    //   title: 'Material',
    //   type: 'collapse',
    //   icon: icons.Inventory2OutlinedIcon,
    //   children: [
    //     {
    //       id: 'Material-Stock',
    //       title: 'Material Stock',
    //       type: 'item',
    //       url: '/material/Material-Stock',
    //       breadcrumbs: false
    //     },
    //     {
    //       id: 'RawMaterialStock',
    //       title: 'Raw Material Stock',
    //       type: 'item',
    //       url: '/material/RawMaterialStock',
    //       breadcrumbs: false
    //     },
    //     {
    //       id: 'RawMaterialSupplier',
    //       title: 'Raw Material Supplier',
    //       type: 'item',
    //       url: '/material/RawMaterialSupplier',
    //       breadcrumbs: false
    //     }
    //   ]
    // },
    // {
    //   id: 'delivery',
    //   title: 'Delivery',
    //   type: 'collapse',
    //   icon: icons.LocalShippingOutlinedIcon,
    //   children: [
    //     {
    //       id: 'MannerDelivery',
    //       title: 'Manner Delivery',
    //       type: 'item',
    //       url: '/delivery/MannerDelivery',
    //       breadcrumbs: false
    //     },
    //     {
    //       id: 'MedicationDelivery',
    //       title: 'Medication Delivery',
    //       type: 'item',
    //       url: '/delivery/MedicationDelivery',
    //       breadcrumbs: false
    //     },
    //     {
    //       id: 'TransportationFeedDelivery',
    //       title: 'Transportation Delivery',
    //       type: 'item',
    //       url: '/delivery/TransportationFeedDelivery',
    //       breadcrumbs: false
    //     }
    //     // {
    //     //   id: 'brands',
    //     //   title: 'Brands',
    //     //   type: 'item',
    //     //   url: '/delivery/brands',
    //     //   breadcrumbs: false
    //     // }
    //   ]
    // }
  ]
};

export default utilities;
